
package conecciones;

import java.sql.*;

public class Conexion {
    private static final String URL="jdbc:mysql://localhost:3306/alquilerc?useSSL=false&useTimezone=true&serverTimezone=America/Mexico_City&allowPublicKeyRetrieval=true";
    private static final String user="root";
    private static final String pass="";
            
    public static Connection getConexion(){
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, user, pass);
        } catch (SQLException ex) {
            System.out.println("Error al conectar con la base de datos");
        }
        return con;
    }
    
    public static void close(PreparedStatement stmt){
        try {
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Error al cerrar el PreparedStatement");
        }
    }
    
    public static void close(ResultSet rs){
        try {
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Error al cerrar el Resulset");
        }
    }
    
    public static void close(Connection con){
        try {
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error al cerrar la conexion");
        }
    }
}
