package identidades;

public class Coche {
    private int idCoche;
    private String placa;
    private String modelo;
    private String marca;
    private double precioDias;

    public Coche() {
    }

    public Coche(int idCoche) {
        this.idCoche = idCoche;
    }

    public Coche(int idCoche, String placa, String modelo, String marca, double precioDias) {
        this.idCoche = idCoche;
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.precioDias = precioDias;
    }

    public Coche(String placa, String modelo, String marca, double precioDias) {
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.precioDias = precioDias;
    }

    public int getIdCoche() {
        return idCoche;
    }

    public void setIdCoche(int idCoche) {
        this.idCoche = idCoche;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getPrecioDias() {
        return precioDias;
    }

    public void setPrecioDias(double precioDias) {
        this.precioDias = precioDias;
    }

    @Override
    public String toString() {
        return "Coche{" + "idCoche=" + idCoche + ", placa=" + placa + ", modelo=" + modelo + ", marca=" + marca + ", precioDias=" + precioDias + '}';
    }
    
}
