/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import conecciones.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Kama
 */
public class InformeDAO {

    private static final String SQL_SELECT = "SELECT * FROM informe";
    private static final String SQL_SELECT_INDIVIDUAL = "SELECT idInfo, TotalPagar, Fecha, idCliente, idCoche FROM informe WHERE idInfo=?";
    private static final String SQL_INSERT = "INSERT INTO informe(TotalPagar, Fecha, idCliente, idCoche) VALUES(?, ?, ? ,?)";
    private static final String SQL_INSERT_ID = "INSERT INTO informe(idInfo, TotalPagar, Fecha, idCliente, idCoche) VALUES(?, ?, ?, ? ,?)";
    private static final String SQL_UPDATE = "UPDATE informe SET TotalPagar=?, Fecha=?, idCliente=?, idCoche=? WHERE idInfo=?";
    private static final String SQL_DELETE = "DELETE FROM informe WHERE idInfo=?";
    private static final String SQL_AUTO_INCREMENT = "SELECT MAX(idInfo) FROM informe";

    public static int siguiente_id() {
        int id = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_AUTO_INCREMENT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return id;
    }

    public static int NRegistros() {
        ArrayList<Informe> informes = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Informe informe = new Informe(rs.getInt("idInfo"),
                        rs.getFloat("TotalPagar"),
                        rs.getDate("Fecha"),
                        rs.getInt("idCliente"),
                        rs.getInt("idCoche"));
                informes.add(informe);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return informes.size();
    }

    public static Informe select_individual(String id) {
        Informe informe = null;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT_INDIVIDUAL);
            stmt.setString(1, id);//al orden de la instruccion de atributo
            rs = stmt.executeQuery();

            while (rs.next()) {
                informe = new Informe(rs.getInt("idInfo"),
                        rs.getFloat("TotalPagar"),
                        rs.getDate("Fecha"),
                        rs.getInt("idCliente"),
                        (rs.getInt("idCoche")));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return informe;
    }

    public static ArrayList<Informe> select() {
        ArrayList<Informe> informes = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Informe informe = new Informe(rs.getInt("idInfo"),
                        rs.getFloat("TotalPagar"),
                        rs.getDate("Fecha"),
                        rs.getInt("idCliente"),
                        rs.getInt("idCoche"));
                informes.add(informe);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return informes;
    }

    public static int insert(Informe informe) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT);

            stmt.setFloat(1, informe.getTotalPagar());//al orden de la instruccion de atributo
            stmt.setDate(2, informe.getFecha());
            stmt.setInt(3, informe.getIdCliente());
            stmt.setInt(4, informe.getIdCoche());

            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int insert_id(Informe informe) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT_ID);

            stmt.setInt(1, informe.getIdInforme());
            stmt.setFloat(2, informe.getTotalPagar());//al orden de la instruccion de atributo
            stmt.setDate(3, informe.getFecha());
            stmt.setInt(4, informe.getIdCliente());
            stmt.setInt(5, informe.getIdCoche());

            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int update(Informe informe) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_UPDATE);

            stmt.setFloat(1, informe.getTotalPagar());//al orden de la instruccion de atributo
            stmt.setDate(2, informe.getFecha());
            stmt.setInt(3, informe.getIdCliente());
            stmt.setInt(4, informe.getIdCoche());

            stmt.setInt(5, informe.getIdInforme());

            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int delete(Informe informe) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_DELETE);
            stmt.setInt(1, informe.getIdInforme());
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
}
