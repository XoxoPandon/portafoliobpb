/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import conecciones.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Kama
 */
public class AlquilerDAO {
    private static final String SQL_SELECT = "SELECT * FROM alquiler";
    private static final String SQL_INSERT = "INSERT INTO alquiler(FechaAlquilerIni, FechaAlquilerFin, idInfo, idEmpleado) VALUES(?, ?, ?, ?)";
    private static final String SQL_INSERT_ID = "INSERT INTO alquiler(idAlq, FechaAlquilerIni, FechaAlquilerFin, idInfo, idEmpleado) VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_INDIVIDUAL = "SELECT idAlq, FechaAlquilerIni, FechaAlquilerFin, idInfo, idEmpleado FROM alquiler WHERE idAlq=?";
    private static final String SQL_UPDATE = "UPDATE alquiler SET FechaAlquilerIni=?, FechaAlquilerFin=?, idInfo=?, idEmpleado=? WHERE idAlq=?";
    private static final String SQL_DELETE = "DELETE FROM alquiler WHERE idAlq=?";
    private static final String SQL_AUTO_INCREMENT = "SELECT MAX(idAlq) FROM alquiler";
    
    public static int siguiente_id() {
        int id = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_AUTO_INCREMENT);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }
        
        return id;
    }
    
    public static int NRegistros() {
        ArrayList<Alquiler> alquileres = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Alquiler alquiler = new Alquiler(rs.getInt("idAlq"),
                        rs.getDate("FechaAlquilerIni"),
                        rs.getDate("FechaAlquilerFin"),
                        rs.getInt("idInfo"),
                        rs.getInt("idEmpleado"));
                alquileres.add(alquiler);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return alquileres.size();
    }
    
    public static Alquiler select_individual(String id) {
        Alquiler alquiler = null;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT_INDIVIDUAL);
            stmt.setString(1, id);//al orden de la instruccion de atributo
            rs = stmt.executeQuery();

            while (rs.next()) {
                alquiler = new Alquiler(rs.getInt("idAlq"),
                        rs.getDate("FechaAlquilerIni"),
                        rs.getDate("FechaAlquilerFin"),
                        rs.getInt("idInfo"),
                        (rs.getInt("idEmpleado")));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return alquiler;
    }
    
    public static ArrayList<Alquiler> select() {
        ArrayList<Alquiler> alquileres = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Alquiler alquiler = new Alquiler(rs.getInt("idAlq"),
                        rs.getDate("FechaAlquilerIni"),
                        rs.getDate("FechaAlquilerFin"),
                        rs.getInt("idInfo"),
                        rs.getInt("idEmpleado"));
                alquileres.add(alquiler);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return alquileres;
    }
    
    public static int insert_id(Alquiler alquiler) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT_ID);
            
            stmt.setInt(1, alquiler.getIdAlquiler());
            stmt.setDate(2, alquiler.getFechaInicio());//al orden de la instruccion de atributo
            stmt.setDate(3, alquiler.getFechaFinal());
            stmt.setInt(4, alquiler.getIdInforme());
            stmt.setInt(5, alquiler.getIdEmpleado());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int insert(Alquiler alquiler) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT);
            
            stmt.setDate(1, alquiler.getFechaInicio());//al orden de la instruccion de atributo
            stmt.setDate(2, alquiler.getFechaFinal());
            stmt.setInt(3, alquiler.getIdInforme());
            stmt.setInt(4, alquiler.getIdEmpleado());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int update(Alquiler alquiler) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_UPDATE);

            stmt.setDate(1, alquiler.getFechaInicio());//al orden de la instruccion de atributo
            stmt.setDate(2, alquiler.getFechaFinal());
            stmt.setInt(3, alquiler.getIdInforme());
            stmt.setInt(4, alquiler.getIdEmpleado());
            
            stmt.setInt(5, alquiler.getIdAlquiler());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
    
    public static int delete(Alquiler alquiler) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_DELETE);
            stmt.setInt(1, alquiler.getIdAlquiler());
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
}