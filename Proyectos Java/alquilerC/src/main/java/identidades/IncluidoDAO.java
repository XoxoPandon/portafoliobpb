/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import conecciones.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Kama
 */
public class IncluidoDAO {
    private static final String SQL_SELECT = "SELECT * FROM incluido";
    private static final String SQL_SELECT_INDIVIDUAL = "SELECT idCoche, idAlquiler, CantDias FROM incluido WHERE idCoche=? AND idAlquiler=?";
    private static final String SQL_INSERT = "INSERT INTO incluido(idCoche, idAlquiler, CantDias) VALUES(?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE incluido SET CantDias=? WHERE idCoche=? AND idAlquiler=?";
    private static final String SQL_DELETE = "DELETE FROM incluido WHERE idCoche=? AND idAlquiler=?";
    
    public static int NRegistros() {
        ArrayList<Incluido> lista = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Incluido incluido = new Incluido(rs.getInt("idCoche"),
                        rs.getInt("idAlquiler"),
                        rs.getInt("CantDias"));
                lista.add(incluido);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return lista.size();
    }
    
    public static Incluido select_individual(String idCoche, String idAlquiler) {
        Incluido incluido = null;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT_INDIVIDUAL);
            stmt.setString(1, idCoche);//al orden de la instruccion de atributo
            stmt.setString(2, idAlquiler);
            rs = stmt.executeQuery();

            while (rs.next()) {
                incluido = new Incluido(rs.getInt("idCoche"),
                        rs.getInt("idAlquiler"),
                        rs.getInt("CantDias"));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return incluido;
    }
    
    public static ArrayList<Incluido> select() {
        ArrayList<Incluido> lista = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Incluido incluido = new Incluido(rs.getInt("idCoche"),
                        rs.getInt("idAlquiler"),
                        rs.getInt("CantDias"));
                lista.add(incluido);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return lista;
    }

    public static int insert(Incluido incluido) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT);
            
            stmt.setInt(1, incluido.getIdCoche());//al orden de la instruccion de atributo
            stmt.setInt(2, incluido.getIdAlquiler());
            stmt.setInt(3, incluido.getCantidadDias());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int update(Incluido incluido) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_UPDATE);

            stmt.setInt(1, incluido.getCantidadDias());
            
            stmt.setInt(2, incluido.getIdCoche());
            stmt.setInt(3, incluido.getIdAlquiler());//al orden de la instruccion de atributo
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
    
    public static int delete(Incluido incluido) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_DELETE);
            stmt.setInt(1, incluido.getIdCoche());
            stmt.setInt(2, incluido.getIdAlquiler());//al orden de la instruccion de atributo
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
}