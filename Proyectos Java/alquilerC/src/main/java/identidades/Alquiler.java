/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import java.sql.Date;

/**
 *
 * @author Kama
 */
public class Alquiler {
    private int idAlquiler;
    private Date FechaInicio;
    private Date FechaFinal;
    private int idInforme;
    private int idEmpleado;

    public Alquiler() {
    }

    public Alquiler(int idAlquiler, Date FechaInicio, Date FechaFinal, int idInforme, int idEmpleado) {
        this.idAlquiler = idAlquiler;
        this.FechaInicio = FechaInicio;
        this.FechaFinal = FechaFinal;
        this.idInforme = idInforme;
        this.idEmpleado = idEmpleado;
    }

    public Alquiler(Date FechaInicio, Date FechaFinal, int idInforme, int idEmpleado) {
        this.FechaInicio = FechaInicio;
        this.FechaFinal = FechaFinal;
        this.idInforme = idInforme;
        this.idEmpleado = idEmpleado;
    }

    public int getIdAlquiler() {
        return idAlquiler;
    }

    public void setIdAlquiler(int idAlquiler) {
        this.idAlquiler = idAlquiler;
    }

    public Date getFechaInicio() {
        return FechaInicio;
    }

    public void setFechaInicio(Date FechaInicio) {
        this.FechaInicio = FechaInicio;
    }

    public Date getFechaFinal() {
        return FechaFinal;
    }

    public void setFechaFinal(Date FechaFinal) {
        this.FechaFinal = FechaFinal;
    }

    public int getIdInforme() {
        return idInforme;
    }

    public void setIdInforme(int idInforme) {
        this.idInforme = idInforme;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    @Override
    public String toString() {
        return "Alquiler{" + "idAlquiler=" + idAlquiler + ", FechaInicio=" + FechaInicio + ", FechaFinal=" + FechaFinal + ", idInforme=" + idInforme + ", idEmpleado=" + idEmpleado + '}';
    }
    
}
