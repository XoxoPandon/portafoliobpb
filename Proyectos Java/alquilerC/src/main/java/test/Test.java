/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import Frames.FrmOpciones;
import identidades.*;
import conecciones.*;
import java.sql.Connection;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

public class Test {

    public static void main(String[] args) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmOpciones().setVisible(true);
            }
        });
        
//================================
//        SELECT
//        CocheDAO dao = new CocheDAO();
//            System.out.println(dao.NRegistros());//Solo aumentas + 1 para tener el IDUnico
//        ArrayList<Coche> coches = dao.select();
//        
//        coches.forEach(coche->{
//        System.out.println(coche.toString());});
//===============================
//        INSERT
//        CocheDAO dao = new CocheDAO();
//        var precio = 1000.00;
//        Coche coche1 = new Coche(29,"camaron","camaron","camaron",precio);
//        dao.insert_id(coche1);
//===============================
//        UPDATE
//        CocheDAO dao = new CocheDAO();
//        var precio = 1000.00;
//        Coche coche1 = new Coche(21,"camaron","camaron","camaron",precio);
//        dao.update(coche1);
//===============================
//        DELETE
//        CocheDAO dao = new CocheDAO();
//        var precio = 1000.00;
//        Coche coche1 = new Coche(21,"camaron","camaron","camaron",precio);
//        dao.delete(coche1);

////## Clientes ##      
//          ClienteDAO dao = new ClienteDAO();
//            System.out.println(dao.NRegistros());//Solo aumentas + 1 para tener el IDUnico
//          ArrayList<Cliente> clientes = dao.select();
//          clientes.forEach(cliente->{
//          System.out.println(cliente.toString());});
//          Cliente cliente = new Cliente(400,"Alejandro","a","a",Long.parseLong("2462047540"));
//          Cliente cliente = new Cliente(228);
//          dao.insert_id(cliente);
////## Empleado ##      
//          EmpleadoDAO dao = new EmpleadoDAO();
//          Empleado empleado = new Empleado(200,"Alejandro","Gonzalez","a",Long.parseLong("2462047540"));
//          dao.insert_id(empleado);
//          System.out.println(dao.NRegistros());//Solo aumentas + 1 para tener el IDUnico
//          ArrayList<Empleado> empleados = dao.select();
//          empleados.forEach(empleado->{
//          System.out.println(empleado.toString());});
//          Empleado empleado = new Empleado(5,"Alejandro","Gonzalez","a",Long.parseLong("2462047540"));
//          Empleado empleado = new Empleado(5);
//          dao.update(empleado);
////## Informes
//        InformeDAO dao = new InformeDAO();
//        System.out.println(dao.NRegistros());//Solo aumentas + 1 para tener el IDUnico
//        ArrayList<Informe> informes = dao.select();
//        informes.forEach(empleado->{
//            System.out.println(empleado.toString());});
//=========================
//        Date date = Date.valueOf("2020-01-21");//Se crea un objeto de tipo date para guardar la fecha en String
//        java.sql.Date fecha = new java.sql.Date(date.getTime());//Se convierte el date en formato sql.Date
//        Informe informe = new Informe(1000,Float.parseFloat("1500.00"),fecha ,199,20);//Y se sube en el orden de los campos
//        dao.insert_id(informe);
//=========================
//        Date date = Date.valueOf("2020-10-21");//Se crea un objeto de tipo date para guardar la fecha en String
//        java.sql.Date fecha = new java.sql.Date(date.getTime());//Se convierte el date en formato sql.Date
//        Informe informe = new Informe(201,Float.parseFloat("1200.00"),fecha ,199);//Y se sube en el orden de los campos
//        dao.update(informe);
//        dao.delete(informe);
////## Alquiler
//        AlquilerDAO dao = new AlquilerDAO();
//        System.out.println(dao.NRegistros());//Solo aumentas + 1 para tener el IDUnico
//        ArrayList<Alquiler> alquileres = dao.select();
//        alquileres.forEach(alquiler->{System.out.println(alquiler.toString());});
//================================
//        Date date = Date.valueOf("2020-10-21");//Se crea un objeto de tipo date para guardar la fecha en String
//        java.sql.Date FechaInicio = new java.sql.Date(date.getTime());//Se convierte el date en formato sql.Date
//        Date date2 = Date.valueOf("2020-10-21");//Se crea un objeto de tipo date para guardar la fecha en String
//        java.sql.Date FechaFinal = new java.sql.Date(date2.getTime());//Se convierte el date en formato sql.Date
//        
//        Alquiler alquiler = new Alquiler(301,FechaInicio, FechaFinal, 200, 1);
//        dao.insert_id(alquiler);
//////## Incluido
  //      IncluidoDAO dao = new IncluidoDAO();
//System.out.println(dao.NRegistros());//Solo aumentas + 1 para tener el IDUnico
//        ArrayList<Incluido> lista = dao.select();
//        lista.forEach(x->{System.out.println(x.toString());});
//        Incluido incluido = new Incluido(20, 202, 10);
//        System.out.println(dao.delete(incluido)); //Todas las funciones DAO retornan un valor
        //En el caso de select retorna un ArrayList
        //En el caso de insert, update y delete retornan un entero, el cual son el total de registros modificados. (Caso util para revisar en fa si modifico mas de uno o valio verga)
    }
}
