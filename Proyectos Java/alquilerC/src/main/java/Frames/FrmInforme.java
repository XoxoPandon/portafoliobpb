/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import com.toedter.calendar.JDateChooser;
import identidades.Alquiler;
import identidades.AlquilerDAO;
import identidades.Cliente;
import identidades.ClienteDAO;
import identidades.Coche;
import identidades.CocheDAO;
import identidades.Empleado;
import identidades.EmpleadoDAO;
import identidades.Incluido;
import identidades.IncluidoDAO;
import identidades.Informe;
import identidades.InformeDAO;
import static java.lang.Character.isLetter;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.util.Date.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 * @author FranciscoJimenez
 */
public class FrmInforme extends javax.swing.JFrame {

    public static final int IDEMP = 0;
    public static final int IDCOCHE = 0;
    public static final int IDALQUI = 0;
    public float precioTotal = 0;
    Date dateAc;
    Date dateFin = null;
    LocalDate fechaAc;
    SimpleDateFormat Formato = new SimpleDateFormat("dd-MM-yyyy");
    int daysdif = 0;
    DefaultTableModel modelo;
    public static String idAlqui = "";

    /**
     * Creates new form FrmInforme
     */
    public FrmInforme() {
        initComponents();
        ConsultaEmp();
        ConsultaCoche();

        fechaAc = LocalDate.now();
        dateAc = Date.valueOf(fechaAc);
        FechaAc.setText(fechaAc.toString());
        actualizar();
        this.setLocationRelativeTo(null);
    }

    public void limpiar() {
        TabTodo.clearSelection();
        NomClie.setText("");
        ApClie.setText("");
        AmClie.setText("");
        TelClie.setText("");

        ComEmp.setSelectedIndex(0);
        ComCoche.setSelectedIndex(0);

        FechaFin.setCalendar(null);

        TotalP.setText("0.0");
        fechaAc = LocalDate.now();
        dateAc = Date.valueOf(fechaAc);
        FechaAc.setText(fechaAc.toString());
        btnRentar.setEnabled(true);
        btnEditar.setEnabled(false);
        btnBorrar.setEnabled(false);

    }

    public void ConsultaEmp() {
        try {
            ComEmp.removeAllItems();

            EmpleadoDAO empdao = new EmpleadoDAO();
            ArrayList<Empleado> listEmp = empdao.select();

            for (int i = 0; i < listEmp.size(); i++) {
                ComEmp.addItem(String.valueOf(listEmp.get(i).getIdEmpleado()) + " - " + String.valueOf(listEmp.get(i).getNombre())
                        + " " + String.valueOf(listEmp.get(i).getApellidoP()));
            }

        } catch (IndexOutOfBoundsException ioobe) {
            System.out.println("ya valio queso esta vaina");
        }
    }

    public void ConsultaCoche() {
        try {
            ComCoche.removeAllItems();
            CocheDAO cochedao = new CocheDAO();
            ArrayList<Coche> listCoche = cochedao.select();

            for (int i = 0; i < listCoche.size(); i++) {
                ComCoche.addItem(String.valueOf(listCoche.get(i).getIdCoche()) + " - " + String.valueOf(listCoche.get(i).getMarca())
                        + " " + String.valueOf(listCoche.get(i).getModelo()) + "  " + String.valueOf(listCoche.get(i).getPlaca())
                        + "     $" + String.valueOf(listCoche.get(i).getPrecioDias()));
            }

        } catch (IndexOutOfBoundsException ioobe) {
            System.out.println("ya valio queso esta vaina");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        NomClie = new javax.swing.JTextField();
        AmClie = new javax.swing.JTextField();
        ApClie = new javax.swing.JTextField();
        TelClie = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        ComCoche = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        ComEmp = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        FechaAc = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        FechaFin = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        TotalP = new javax.swing.JTextField();
        btnRentar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        TabTodo = new javax.swing.JTable();
        btnCancelar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        btnRegresar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(17, 20, 26));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nombre:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Apellido Paterno:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Apellido Materno:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Número de Teléfono:");

        NomClie.setBackground(new java.awt.Color(17, 20, 26));
        NomClie.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        NomClie.setForeground(new java.awt.Color(255, 255, 255));

        AmClie.setBackground(new java.awt.Color(17, 20, 26));
        AmClie.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        AmClie.setForeground(new java.awt.Color(255, 255, 255));

        ApClie.setBackground(new java.awt.Color(17, 20, 26));
        ApClie.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        ApClie.setForeground(new java.awt.Color(255, 255, 255));

        TelClie.setBackground(new java.awt.Color(17, 20, 26));
        TelClie.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        TelClie.setForeground(new java.awt.Color(255, 255, 255));
        TelClie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TelClieKeyTyped(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Coche");

        ComCoche.setBackground(new java.awt.Color(0, 156, 231));
        ComCoche.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        ComCoche.setForeground(new java.awt.Color(0, 156, 231));
        ComCoche.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ComCoche.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ComCocheItemStateChanged(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Empleado");

        ComEmp.setBackground(new java.awt.Color(0, 156, 231));
        ComEmp.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        ComEmp.setForeground(new java.awt.Color(0, 156, 231));
        ComEmp.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ComEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComEmpActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Fecha Entrega");

        FechaAc.setBackground(new java.awt.Color(17, 20, 26));
        FechaAc.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        FechaAc.setForeground(new java.awt.Color(255, 255, 255));
        FechaAc.setEnabled(false);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Fecha Devolución");

        FechaFin.setBackground(new java.awt.Color(17, 20, 26));
        FechaFin.setForeground(new java.awt.Color(255, 255, 255));
        FechaFin.setDateFormatString("yyyy-MM-dd");
        FechaFin.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                FechaFinPropertyChange(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Total a Pagar");

        TotalP.setBackground(new java.awt.Color(17, 20, 26));
        TotalP.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        TotalP.setForeground(new java.awt.Color(255, 255, 255));
        TotalP.setEnabled(false);

        btnRentar.setBackground(new java.awt.Color(14, 137, 206));
        btnRentar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnRentar.setText("Agregar Renta");
        btnRentar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRentarActionPerformed(evt);
            }
        });

        TabTodo.setBackground(new java.awt.Color(17, 20, 26));
        TabTodo.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        TabTodo.setForeground(new java.awt.Color(255, 255, 255));
        TabTodo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nombre", "FechaInicio", "FechaFin", "Empleado", "Coche", "Total_a_pagar"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TabTodo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabTodoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TabTodo);

        btnCancelar.setBackground(new java.awt.Color(14, 137, 206));
        btnCancelar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnCancelar.setText("Cancelar Renta");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnEditar.setBackground(new java.awt.Color(14, 137, 206));
        btnEditar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnEditar.setText("Editar Renta");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnBorrar.setBackground(new java.awt.Color(14, 137, 206));
        btnBorrar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnBorrar.setText("Borrar Renta");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });

        btnRegresar.setBackground(new java.awt.Color(14, 137, 206));
        btnRegresar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnRegresar.setForeground(new java.awt.Color(255, 255, 255));
        btnRegresar.setText("Regresar");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TelClie, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(51, 51, 51)
                                .addComponent(jLabel1))
                            .addComponent(NomClie, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AmClie, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabel3))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel4))
                            .addComponent(ApClie, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel2))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(btnRentar)
                        .addGap(87, 87, 87)
                        .addComponent(btnEditar)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 81, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(138, 138, 138)
                                    .addComponent(TotalP, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(128, 128, 128))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel6)
                                            .addGap(147, 147, 147))
                                        .addComponent(ComCoche, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(FechaAc, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel8))
                                                .addGap(55, 55, 55)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jLabel9))
                                                    .addComponent(FechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel7)
                                                .addGap(151, 151, 151))
                                            .addComponent(ComEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(130, 130, 130)
                                .addComponent(jLabel5)))
                        .addGap(119, 119, 119))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addGap(73, 73, 73))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRegresar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRegresar)
                .addGap(122, 122, 122)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NomClie, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, 0)
                        .addComponent(ComEmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ComCoche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(FechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(FechaAc, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ApClie, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(AmClie, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(49, 49, 49)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TotalP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TelClie, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(102, 102, 102)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRentar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(723, 723, 723))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 843, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void actualizar() {
        btnEditar.setEnabled(false);
        btnBorrar.setEnabled(false);
        try {

            String titulos[] = {"ID", "Cliente", "Fecha", "Fecha fin", "Empleado", "Coche", "Total"};

            modelo = new DefaultTableModel(null, titulos) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    if (column == 7) {
                        return true;
                    } else {
                        return false;
                    }
                }
            };

            TabTodo.setModel(modelo);

            EmpleadoDAO empdao = new EmpleadoDAO();
            CocheDAO cochedao = new CocheDAO();
            ClienteDAO clientedao = new ClienteDAO();
            InformeDAO informedao = new InformeDAO();
            AlquilerDAO alquilerdao = new AlquilerDAO();

            ArrayList<Empleado> listEmp = empdao.select();
            ArrayList<Coche> listCoche = cochedao.select();
            ArrayList<Cliente> listClien = clientedao.select();
            ArrayList<Informe> listInfo = informedao.select();
            ArrayList<Alquiler> listAlq = alquilerdao.select();

            String[] filas = new String[7];
            for (int i = 0; i < listInfo.size(); i++) {
                filas[0] = String.valueOf(listInfo.get(i).getIdInforme());

                for (int j = 0; j < listClien.size(); j++) {
                    if (listClien.get(j).getIdCliente() == listInfo.get(i).getIdCliente()) {
                        filas[1] = listClien.get(j).getNombre() + " " + listClien.get(j).getApellidoP();
                    }
                }

                for (int j = 0; j < listAlq.size(); j++) {
                    if (listAlq.get(j).getIdInforme() == listInfo.get(i).getIdInforme()) {
                        filas[2] = String.valueOf(listAlq.get(j).getFechaInicio());
                        filas[3] = String.valueOf(listAlq.get(j).getFechaFinal());
                        for (int k = 0; k < listEmp.size(); k++) {
                            if (listEmp.get(k).getIdEmpleado() == listAlq.get(j).getIdEmpleado()) {
                                filas[4] = listEmp.get(k).getNombre() + " " + listEmp.get(k).getApellidoP();
                            }
                        }
                    }
                }

                for (int j = 0; j < listCoche.size(); j++) {
                    if (listCoche.get(j).getIdCoche() == listInfo.get(i).getIdCoche()) {
                        filas[5] = listCoche.get(j).getMarca() + " " + listCoche.get(j).getModelo() + " " + listCoche.get(j).getPlaca();
                    }
                }

                filas[6] = String.valueOf(listInfo.get(i).getTotalPagar());

                //Final
                modelo.addRow(filas);
            }

            //TableColumn id = TabTodo.getColumn("idEmpleado");
        } catch (IndexOutOfBoundsException ioobe) {
            System.out.println("Error en actualizar");
        }
    }

    private void btnRentarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRentarActionPerformed
        try {
            java.util.Date date = FechaFin.getDate();

            if (NomClie.getText().isEmpty() || ApClie.getText().isEmpty() || AmClie.getText().isEmpty() || TelClie.getText().isEmpty()
                    || date == null) {
                JOptionPane.showMessageDialog(null, "Necesitas rellenar todos los campos");
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                //format es la fecha fin
                String format = formatter.format(date);
                System.out.println(format);

                ClienteDAO clientedao = new ClienteDAO();
                InformeDAO informedao = new InformeDAO();
                AlquilerDAO alquilerdao = new AlquilerDAO();
                IncluidoDAO incluidodao = new IncluidoDAO();

                int idCliente = clientedao.siguiente_id();
                int idInforme = informedao.siguiente_id();
                int idAlquiler = alquilerdao.siguiente_id();

                Cliente cli = new Cliente(idCliente, NomClie.getText().toString(),
                        ApClie.getText().toString(), AmClie.getText().toString(),
                        Long.parseLong(TelClie.getText().toString()));

                String datoscoche[] = ComCoche.getModel().getSelectedItem().toString().split(" -");

                Informe info = new Informe(idInforme, precioTotal,
                        Date.valueOf(format), idCliente, Integer.parseInt(datoscoche[0]));

                String datosemple[] = ComEmp.getModel().getSelectedItem().toString().split(" -");

                Alquiler alq = new Alquiler(idAlquiler, dateAc, Date.valueOf(format), idInforme, Integer.parseInt(datosemple[0]));

                Incluido inclu = new Incluido(Integer.parseInt(datoscoche[0]), idAlquiler, daysdif);

                clientedao.insert_id(cli);
                informedao.insert_id(info);
                alquilerdao.insert_id(alq);
                incluidodao.insert(inclu);

                JOptionPane.showMessageDialog(null, "Se inserto correctamente");
                actualizar();
                limpiar();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }//GEN-LAST:event_btnRentarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        try {
            int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Alerta!", JOptionPane.YES_NO_OPTION);
            
            if(resp==0){
            java.util.Date date = FechaFin.getDate();

            if (NomClie.getText().isEmpty() || ApClie.getText().isEmpty() || AmClie.getText().isEmpty() || TelClie.getText().isEmpty()
                    || date == null) {
                JOptionPane.showMessageDialog(null, "Necesitas rellenar todos los campos");
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                //format es la fecha fin
                String format = formatter.format(date);
                System.out.println(format);

                AlquilerDAO alquilerdao = new AlquilerDAO();
                ClienteDAO clientedao = new ClienteDAO();
                InformeDAO informedao = new InformeDAO();
                IncluidoDAO incluidodao = new IncluidoDAO();
                        
                Alquiler alq = alquilerdao.select_individual(idAlqui);
                Informe info = informedao.select_individual(String.valueOf(alq.getIdInforme()));
                Cliente cli = clientedao.select_individual(String.valueOf(info.getIdCliente()));
                
                
                Cliente cliente = new Cliente(cli.getIdCliente(), NomClie.getText().toString(),
                        ApClie.getText().toString(), AmClie.getText().toString(),
                        Long.parseLong(TelClie.getText().toString()));

                String datoscoche[] = ComCoche.getModel().getSelectedItem().toString().split(" -");

                Informe informe = new Informe(info.getIdInforme(),Float.parseFloat(TotalP.getText().toString()),
                        Date.valueOf(format), cli.getIdCliente(), Integer.parseInt(datoscoche[0]));

                String datosemple[] = ComEmp.getModel().getSelectedItem().toString().split(" -");

                Alquiler alquiler = new Alquiler(alq.getIdAlquiler(), alq.getFechaInicio(), Date.valueOf(format), info.getIdInforme(), 
                        Integer.parseInt(datosemple[0]));

                Incluido inclu = new Incluido(Integer.parseInt(datoscoche[0]), alq.getIdAlquiler(), daysdif);

                clientedao.update(cliente);
                informedao.update(informe);
                alquilerdao.update(alquiler);
                incluidodao.update(inclu);

                JOptionPane.showMessageDialog(null, "Se inserto correctamente");
                actualizar();
                limpiar();
                btnRentar.setEnabled(true);
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_btnEditarActionPerformed

    private void FechaFinPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_FechaFinPropertyChange

        try {
            java.util.Date date = FechaFin.getDate();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String format = formatter.format(date);
            System.out.println(format);

            LocalDate dbAfter = LocalDate.parse(format, DateTimeFormatter.ISO_DATE);

            daysdif = (int) ChronoUnit.DAYS.between(fechaAc, dbAfter);

            String precio = ComCoche.getModel().getSelectedItem().toString();

            int inicio = precio.indexOf("$");
            String obfinal[] = precio.substring(inicio).split("\\$");

            precioTotal = Float.parseFloat(obfinal[1]);

            int xd = (int) daysdif;

            TotalP.setText(String.valueOf(precioTotal * xd));

        } catch (Exception e) {
            System.out.println("Error en actualizar");
        }
    }//GEN-LAST:event_FechaFinPropertyChange

    private void ComCocheItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ComCocheItemStateChanged
        try {//Funcion para calcular el total a pagar al seleccionar el carro
            java.util.Date date = FechaFin.getDate();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String format = formatter.format(date);
            System.out.println(format);

            LocalDate dbAfter = LocalDate.parse(format, DateTimeFormatter.ISO_DATE);

            daysdif = (int) ChronoUnit.DAYS.between(fechaAc, dbAfter);

            String precio = ComCoche.getModel().getSelectedItem().toString();

            int inicio = precio.indexOf("$");
            String obfinal[] = precio.substring(inicio).split("\\$");

            precioTotal = Float.parseFloat(obfinal[1]);

            int xd = (int) daysdif;

            TotalP.setText(String.valueOf(precioTotal * xd));

        } catch (Exception e) {

        }
    }//GEN-LAST:event_ComCocheItemStateChanged

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Alerta!", JOptionPane.YES_NO_OPTION);
        
        if(resp==0){
            
        AlquilerDAO alquidao = new AlquilerDAO();
        InformeDAO infodao = new InformeDAO();
        ClienteDAO cliedao = new ClienteDAO();
        
        Alquiler alquiler = alquidao.select_individual(idAlqui);
        Informe informe = infodao.select_individual(String.valueOf(alquiler.getIdInforme()));
        Cliente cliente = cliedao.select_individual(String.valueOf(informe.getIdCliente()));
        
        cliedao.delete(cliente);   
        limpiar();
        actualizar();
        btnRentar.setEnabled(true);
        }
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        limpiar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void TelClieKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TelClieKeyTyped
        // TODO add your handling code here:
        var validar = evt.getKeyChar();

        if (isLetter(validar)) {
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_TelClieKeyTyped

    private void TabTodoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabTodoMouseClicked

        try {
            int filaSeleccionada = TabTodo.getSelectedRow();
            idAlqui = (String) TabTodo.getValueAt(filaSeleccionada, 0);

            AlquilerDAO alquilerdao = new AlquilerDAO();
            ClienteDAO clientedao = new ClienteDAO();
            CocheDAO cochedao = new CocheDAO();
            InformeDAO informedao = new InformeDAO();
            EmpleadoDAO empleadodao = new EmpleadoDAO();

            ArrayList<Empleado> empleados = empleadodao.select();
            ArrayList<Informe> informes = informedao.select();
            ArrayList<Cliente> clientes = clientedao.select();
            ArrayList<Coche> coches = cochedao.select();

            Alquiler alquiler = alquilerdao.select_individual(idAlqui);
            Empleado empleado;
            Informe informe;
            Cliente cliente;
            Coche coche;

            fechaAc = LocalDate.parse(alquiler.getFechaInicio().toString());
            FechaAc.setText(alquiler.getFechaInicio().toString());
            FechaFin.setDate(alquiler.getFechaFinal());

            for (int i = 0; i < empleados.size(); i++) {
                if (alquiler.getIdEmpleado() == empleados.get(i).getIdEmpleado()) {
                    empleado = empleados.get(i);
                    ComEmp.setSelectedItem(empleado.getIdEmpleado() + " - " + empleado.getNombre() + " " + empleado.getApellidoP());
                }
            }

            for (int i = 0; i < informes.size(); i++) {
                if (alquiler.getIdInforme() == informes.get(i).getIdInforme()) {
                    informe = informes.get(i);
                    TotalP.setText(String.valueOf(informe.getTotalPagar()));
                    for (int j = 0; j < clientes.size(); j++) {
                        if (informe.getIdCliente() == clientes.get(j).getIdCliente()) {
                            cliente = clientes.get(j);
                            NomClie.setText(cliente.getNombre());
                            ApClie.setText(cliente.getApellidoP());
                            AmClie.setText(cliente.getApellidoM());
                            TelClie.setText(String.valueOf(cliente.getTelefono()));
                        }
                    }

                    for (int j = 0; j < coches.size(); j++) {
                        if (informe.getIdCoche() == coches.get(j).getIdCoche()) {
                            coche = coches.get(j);

                            ComCoche.setSelectedItem(String.valueOf(coche.getIdCoche()) + " - " + String.valueOf(coche.getMarca())
                                    + " " + String.valueOf(coche.getModelo()) + "  " + String.valueOf(coche.getPlaca())
                                    + "     $" + String.valueOf(coche.getPrecioDias()));
                        }
                    }
                }
            }

            btnRentar.setEnabled(false);
            btnEditar.setEnabled(true);
            btnBorrar.setEnabled(true);
        } catch (Exception e) {

        }


    }//GEN-LAST:event_TabTodoMouseClicked

    private void ComEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComEmpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComEmpActionPerformed

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        // TODO add your handling code here:
        FrmOpciones emp = new FrmOpciones();
        emp.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnRegresarActionPerformed

    /**
     * @param args the command line arguemnts
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmInforme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmInforme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmInforme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmInforme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmInforme().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AmClie;
    private javax.swing.JTextField ApClie;
    private javax.swing.JComboBox<String> ComCoche;
    private javax.swing.JComboBox<String> ComEmp;
    private javax.swing.JTextField FechaAc;
    private com.toedter.calendar.JDateChooser FechaFin;
    private javax.swing.JTextField NomClie;
    private javax.swing.JTable TabTodo;
    private javax.swing.JTextField TelClie;
    private javax.swing.JTextField TotalP;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JButton btnRentar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
