/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria.interfaces.Cortes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import papeleria.interfaces.PantallaInicio;


public class ConsultarCorte extends javax.swing.JFrame {

    /**
     * Creates new form ConsultarCorte
     */
    
    Connection con = null;
    Statement stmt = null;
    
    String titulos[] = {"ID", "Fecha", "Total en Caja", "Total en Sistema", "Diferencia", "Empleado"};
    String fila[] = new String[6];
    DefaultTableModel modelo;
    
    public ConsultarCorte() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        Consulta();
    }
    
    public void Consulta(){
        try {

            String url = "jdbc:mysql://localhost:3306/Papeleria";
            String usuario = "root";
            String contraseña = "";
            double totalSist = 0;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Cortes");
            String Empleado = "";

            modelo = new DefaultTableModel(null, titulos);

            while (rs.next()) {

                fila[0] = rs.getString("IdCorte");
                fila[1] = rs.getString("Fecha");
                totalSist = (double) obtenerTotalSistema(rs.getString("Fecha"));
                fila[2] = String.format("%.2f", totalSist);
                fila[3] = rs.getString("Total");
                fila[4] = String.format("%.2f", totalSist - Double.parseDouble(rs.getString("Total")));
                Empleado = obtenerEmpleado(rs.getString("IdEmpleado"));
                fila[5] = Empleado;

                modelo.addRow(fila);
            }
            Consultas.setModel(modelo);
            TableColumn id = Consultas.getColumn("ID");
            id.setMaxWidth(100);
            TableColumn fec = Consultas.getColumn("Fecha");
            fec.setMaxWidth(200);
            TableColumn tCaja = Consultas.getColumn("Total en Caja");
            tCaja.setMaxWidth(200);
            TableColumn tSist = Consultas.getColumn("Total en Sistema");
            tSist.setMaxWidth(200);
            TableColumn dif = Consultas.getColumn("Diferencia");
            dif.setMaxWidth(200);            
            TableColumn emp = Consultas.getColumn("Empleado");
            emp.setMaxWidth(200);

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }
    
    public String obtenerEmpleado (String idEmp){
        String Empleado = "";
        try{
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Empleado WHERE IdEmpleado = " + idEmp);

            while (rs.next()) {
                Empleado = rs.getString("IdEmpleado") + " - " + rs.getString("Nombre");
            }            
        } catch (SQLException ex) {
            Logger.getLogger(ConsultarCorte.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
        return Empleado;
    }
    public double obtenerTotalSistema (String Fecha){
        double Total = 0;
        
            try {
                int idVenta = 0;
                stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("select idVenta from Venta WHERE Fecha = '" + Fecha + "'");

                //System.out.println(Fecha);
                while(rs.next()){
                    int idProducto = 0;
                    int cantidad = 0;
                    idVenta = rs.getInt("idVenta");
                    stmt = con.createStatement();
                    ResultSet rs1 = stmt.executeQuery("select * from VenPro WHERE idVenta = " + idVenta); 
                    while(rs1.next()){
                        double precio = 0;
                        idProducto = rs1.getInt("IdProducto");
                        cantidad = rs1.getInt("Cantidad");
                        stmt = con.createStatement();
                        ResultSet rs2 = stmt.executeQuery("select Precio from Producto WHERE idProducto = " + idProducto);
                        while(rs2.next()){
                            precio = rs2.getDouble("Precio");
                        }
                        Total += precio*cantidad;
                        }
                    }
                    //System.out.println(Total);


                } catch (SQLException ex) {
                    Logger.getLogger(RealizarCorte.class.getName()).log(Level.SEVERE, null, ex);
                }
        
                return Total;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        ConsultaCorte2 = new javax.swing.JButton();
        RealizaCorte2 = new javax.swing.JButton();
        Regresar2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Consultas = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        Consulta = new javax.swing.JLabel();
        ConsultaCor = new javax.swing.JTextField();
        Buscar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(102, 153, 255));

        jLabel4.setBackground(new java.awt.Color(0, 0, 0));
        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("CORTES");

        ConsultaCorte2.setBackground(new java.awt.Color(153, 0, 0));
        ConsultaCorte2.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        ConsultaCorte2.setForeground(new java.awt.Color(255, 255, 255));
        ConsultaCorte2.setText("Consulta");
        ConsultaCorte2.setBorder(null);
        ConsultaCorte2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConsultaCorte2ActionPerformed(evt);
            }
        });

        RealizaCorte2.setBackground(new java.awt.Color(153, 0, 0));
        RealizaCorte2.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        RealizaCorte2.setForeground(new java.awt.Color(255, 255, 255));
        RealizaCorte2.setText("Realizar Corte");
        RealizaCorte2.setBorder(null);
        RealizaCorte2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RealizaCorte2ActionPerformed(evt);
            }
        });

        Regresar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Regresar.png"))); // NOI18N
        Regresar2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Regresar2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(Regresar2)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(153, 153, 153)
                        .addComponent(RealizaCorte2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 155, Short.MAX_VALUE)
                        .addComponent(ConsultaCorte2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(139, 139, 139))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(Regresar2)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ConsultaCorte2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(RealizaCorte2, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(0, 153, 102));

        Consultas.setBackground(new java.awt.Color(102, 255, 102));
        Consultas.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        Consultas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Consultas.setFocusable(false);
        Consultas.setGridColor(new java.awt.Color(255, 102, 51));
        Consultas.setSelectionBackground(new java.awt.Color(225, 225, 225));
        Consultas.setSelectionForeground(new java.awt.Color(0, 0, 0));
        jScrollPane1.setViewportView(Consultas);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Consulta");

        Consulta.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Consulta.setForeground(new java.awt.Color(255, 255, 255));
        Consulta.setText("Ingrese la fecha del corte: ");

        ConsultaCor.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        ConsultaCor.setToolTipText("");
        ConsultaCor.setName(""); // NOI18N
        ConsultaCor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConsultaCorActionPerformed(evt);
            }
        });

        Buscar.setBackground(new java.awt.Color(255, 255, 255));
        Buscar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Buscar.setText("Buscar");
        Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("*En formato AAAA-MM-DD");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(136, 136, 136)
                .addComponent(Consulta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(ConsultaCor, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Buscar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ConsultaCor)
                    .addComponent(Consulta)
                    .addComponent(Buscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ConsultaCorte2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConsultaCorte2ActionPerformed
        ConsultarCorte consultarCorte = new ConsultarCorte();
        consultarCorte.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ConsultaCorte2ActionPerformed

    private void RealizaCorte2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RealizaCorte2ActionPerformed
        // TODO add your handling code here:
        RealizarCorte realizarCorte = new RealizarCorte();
        realizarCorte.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_RealizaCorte2ActionPerformed

    private void Regresar2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Regresar2MouseClicked
        PantallaInicio abrir = new PantallaInicio();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_Regresar2MouseClicked

    private void ConsultaCorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConsultaCorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ConsultaCorActionPerformed

    private void BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarActionPerformed
        try {
            stmt = con.createStatement();
            int idB;
            String fecha = ConsultaCor.getText();
            String Empleado = "";
            ResultSet rs;
            double totalSist = 0;

            if("".equals(fecha)){
                rs = stmt.executeQuery("select * from Cortes");
            }

            else {
                rs = stmt.executeQuery("select * from Cortes WHERE Fecha = '" +  fecha + "'");
            }

            modelo = new DefaultTableModel(null, titulos);

            while (rs.next()) {

                fila[0] = rs.getString("IdCorte");
                fila[1] = rs.getString("Fecha");
                totalSist = (double) obtenerTotalSistema(rs.getString("Fecha"));
                fila[2] = String.format("%.2f", totalSist);
                fila[3] = rs.getString("Total");
                fila[4] = String.format("%.2f", totalSist - Double.parseDouble(rs.getString("Total")));
                Empleado = obtenerEmpleado(rs.getString("IdEmpleado"));
                fila[5] = Empleado;

                modelo.addRow(fila);
            }
            Consultas.setModel(modelo);
            TableColumn id = Consultas.getColumn("ID");
            id.setMaxWidth(100);
            TableColumn fec = Consultas.getColumn("Fecha");
            fec.setMaxWidth(200);
            TableColumn tCaja = Consultas.getColumn("Total en Caja");
            tCaja.setMaxWidth(200);
            TableColumn tSist = Consultas.getColumn("Total en Sistema");
            tSist.setMaxWidth(200);
            TableColumn dif = Consultas.getColumn("Diferencia");
            dif.setMaxWidth(200);            
            TableColumn emp = Consultas.getColumn("Empleado");
            emp.setMaxWidth(200);

        } catch (NumberFormatException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }//GEN-LAST:event_BuscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarCorte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarCorte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarCorte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarCorte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new ConsultarCorte().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Buscar;
    private javax.swing.JLabel Consulta;
    private javax.swing.JTextField ConsultaCor;
    private javax.swing.JButton ConsultaCorte2;
    private javax.swing.JTable Consultas;
    private javax.swing.JButton RealizaCorte2;
    private javax.swing.JLabel Regresar2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
