/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria.interfaces.Cortes;

import com.mysql.jdbc.PreparedStatement;
import static java.lang.Character.isLetter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import papeleria.interfaces.PantallaInicio;


public final class RealizarCorte extends javax.swing.JFrame {

    /**
     * Creates new form RealizarCorte
     */
    
    Connection con = null;
    Statement stmt = null;
    PreparedStatement ps;
    
    Date fecha1;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    
    public RealizarCorte() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        fecha1 = new Date();
        Date  DatFecha = fecha1;
        String utilFecha = formato.format(DatFecha);
        obtenerEmpleados();
        this.fecha.setText(utilFecha);
        this.CorteTrue.setVisible(false);
        obtenerTotalVentas();
        
        this.importeTarjeta.setText("0.00");
        compruebaNoHayCorte();

        
    }
    
     public void obtenerEmpleados(){
        try {

            String url = "jdbc:mysql://localhost:3306/Papeleria";
            String usuario = "root";
            String contraseña = "";
            String elim = "";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Empleado");


            while (rs.next()) {
                this.IdEmpleado.addItem(rs.getString("IdEmpleado") + " - " + rs.getString("Nombre"));
            }
                       
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }
     
    public void obtenerTotalVentas(){
        try {
            String Fecha = this.fecha.getText();
            int idVenta = 0;
            double Total = 0;
            double Faltante = Double.parseDouble(this.Faltante.getText());
            double TotalVe = Double.parseDouble(this.TotalV.getText());
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select idVenta from Venta WHERE Fecha = '" + Fecha + "'");

            System.out.println(Fecha);
            while(rs.next()){
                int idProducto = 0;
                int cantidad = 0;
                idVenta = rs.getInt("idVenta");
                //System.out.println(idVenta);
                stmt = con.createStatement();
                ResultSet rs1 = stmt.executeQuery("select * from VenPro WHERE idVenta = " + idVenta); 
                while(rs1.next()){
                    double precio = 0;
                    idProducto = rs1.getInt("IdProducto");
                    cantidad = rs1.getInt("Cantidad");
                    //System.out.println(cantidad);
                    stmt = con.createStatement();
                    ResultSet rs2 = stmt.executeQuery("select Precio from Producto WHERE idProducto = " + idProducto);
                    while(rs2.next()){
                        precio = rs2.getDouble("Precio");
                    }
                    //System.out.println(precio);
                    Total += precio*cantidad;
                    //System.out.println(Total);
                }
            }
            this.TotalSistema.setText(String.valueOf(String.format("%.2f", Total)));
            calcularFaltante();
            //System.out.println(Total);

                       
        } catch (SQLException ex) {
            Logger.getLogger(RealizarCorte.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void calculaTotalCorte(){
        double valor1000 = Double.parseDouble(this.importe1000.getText());
        double valor500 = Double.parseDouble(this.importe500.getText());
        double valor200 = Double.parseDouble(this.importe200.getText());
        double valor100 = Double.parseDouble(this.importe100.getText());
        double valor50 = Double.parseDouble(this.importe50.getText());
        double valor20 = Double.parseDouble(this.importe20.getText());
        double valor10 = Double.parseDouble(this.importe10.getText());
        double valor5 = Double.parseDouble(this.importe5.getText());
        double valor2 = Double.parseDouble(this.importe2.getText());
        double valor1 = Double.parseDouble(this.importe1.getText());
        double valor50c = Double.parseDouble(this.importe50c.getText());
        double valorTarjeta = Double.parseDouble(this.importeTarjeta.getText());

        
        double TotalCorte = valor1000 + valor500 + valor200 + valor100 + valor50 + valor20 + valor10 + valor5 + valor2 + valor1 + valor50c + valorTarjeta;
        this.TotalV.setText(String.valueOf(String.format("%.2f", TotalCorte)));
        
        calcularFaltante();    
    }
    
    public void calcularFaltante(){
        double TotalVe = Double.parseDouble(this.TotalV.getText());
        double Total = Double.parseDouble(this.TotalSistema.getText());
        double faltante = TotalVe - Total;
        if(faltante >=0){
            this.Faltante1.setText("SOBRANTE:");
            this.Faltante.setText(String.valueOf(String.format("%.2f", faltante)));
        }
        
        else{
            faltante *= -1;
            this.Faltante1.setText("FALTANTE:");
            this.Faltante.setText(String.valueOf(String.format("%.2f", faltante)));
        }
        
        
    }
    
    public void compruebaNoHayCorte(){
        try {
            String Fecha = this.fecha.getText();
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Cortes WHERE Fecha = '" + Fecha + "'"); 
            String Fecha1 = "";
            
            while(rs.next()){
                Fecha1 = rs.getString("Fecha");
            }
            if(Fecha1.equals(Fecha)){
                this.CorteTrue.setVisible(true);
                this.EfectuaCorte.setEnabled(false);
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(RealizarCorte.class.getName()).log(Level.SEVERE, null, ex);
        }
                   
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        ConsultaCorte = new javax.swing.JButton();
        RealizaCorte = new javax.swing.JButton();
        Regresar = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        Label1 = new javax.swing.JLabel();
        Label2 = new javax.swing.JLabel();
        Label3 = new javax.swing.JLabel();
        Label4 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        Label5 = new javax.swing.JLabel();
        Label6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        cantidad1000 = new javax.swing.JTextField();
        cantidad500 = new javax.swing.JTextField();
        cantidad200 = new javax.swing.JTextField();
        cantidad100 = new javax.swing.JTextField();
        cantidad50 = new javax.swing.JTextField();
        cantidad20 = new javax.swing.JTextField();
        cantidad10 = new javax.swing.JTextField();
        cantidad5 = new javax.swing.JTextField();
        cantidad2 = new javax.swing.JTextField();
        cantidad1 = new javax.swing.JTextField();
        cantidad50c = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        importe1000 = new javax.swing.JLabel();
        importe500 = new javax.swing.JLabel();
        importe200 = new javax.swing.JLabel();
        importe100 = new javax.swing.JLabel();
        importe50 = new javax.swing.JLabel();
        importe20 = new javax.swing.JLabel();
        importe10 = new javax.swing.JLabel();
        importe5 = new javax.swing.JLabel();
        importe2 = new javax.swing.JLabel();
        importe1 = new javax.swing.JLabel();
        importe50c = new javax.swing.JLabel();
        importeTarjeta = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        NombreC2 = new javax.swing.JLabel();
        TotalV = new javax.swing.JLabel();
        EfectuaCorte = new javax.swing.JButton();
        NombreC3 = new javax.swing.JLabel();
        TotalSistema = new javax.swing.JLabel();
        Faltante1 = new javax.swing.JLabel();
        Faltante = new javax.swing.JLabel();
        NombreC = new javax.swing.JLabel();
        fecha = new javax.swing.JLabel();
        NombreC1 = new javax.swing.JLabel();
        IdEmpleado = new javax.swing.JComboBox<>();
        CorteTrue = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(102, 153, 255));

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("CORTES");

        ConsultaCorte.setBackground(new java.awt.Color(153, 0, 0));
        ConsultaCorte.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        ConsultaCorte.setForeground(new java.awt.Color(255, 255, 255));
        ConsultaCorte.setText("Consulta");
        ConsultaCorte.setBorder(null);
        ConsultaCorte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConsultaCorteActionPerformed(evt);
            }
        });

        RealizaCorte.setBackground(new java.awt.Color(153, 0, 0));
        RealizaCorte.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        RealizaCorte.setForeground(new java.awt.Color(255, 255, 255));
        RealizaCorte.setText("Realizar Corte");
        RealizaCorte.setBorder(null);
        RealizaCorte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RealizaCorteActionPerformed(evt);
            }
        });

        Regresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Regresar.png"))); // NOI18N
        Regresar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                RegresarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(Regresar)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 169, Short.MAX_VALUE)
                        .addComponent(RealizaCorte, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(130, 130, 130)
                        .addComponent(ConsultaCorte, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(139, 139, 139))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(Regresar)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ConsultaCorte, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(RealizaCorte, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));

        jPanel3.setBackground(new java.awt.Color(102, 102, 102));

        Label1.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        Label1.setForeground(new java.awt.Color(255, 255, 255));
        Label1.setText("Denominación");

        Label2.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        Label2.setForeground(new java.awt.Color(255, 255, 255));
        Label2.setText("Tipo");

        Label3.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        Label3.setForeground(new java.awt.Color(255, 255, 255));
        Label3.setText("Cantidad");

        Label4.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        Label4.setForeground(new java.awt.Color(255, 255, 255));
        Label4.setText("Importe");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(Label2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 167, Short.MAX_VALUE)
                .addComponent(Label1)
                .addGap(174, 174, 174)
                .addComponent(Label3)
                .addGap(129, 129, 129)
                .addComponent(Label4)
                .addGap(48, 48, 48))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Label1)
                    .addComponent(Label2)
                    .addComponent(Label3)
                    .addComponent(Label4))
                .addGap(0, 1, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(102, 102, 102));

        Label5.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        Label5.setForeground(new java.awt.Color(255, 255, 255));
        Label5.setText("Tipo");

        Label6.setFont(new java.awt.Font("Times New Roman", 1, 17)); // NOI18N
        Label6.setForeground(new java.awt.Color(255, 255, 255));
        Label6.setText("Importe");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(Label5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 183, Short.MAX_VALUE)
                .addComponent(Label6)
                .addGap(20, 20, 20))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(Label5)
                .addComponent(Label6))
        );

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Efectivo");

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Tarjeta (crédito y débito)");

        cantidad1000.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad1000.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad1000.setText("0");
        cantidad1000.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad1000FocusLost(evt);
            }
        });
        cantidad1000.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad1000ActionPerformed(evt);
            }
        });
        cantidad1000.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad1000KeyTyped(evt);
            }
        });

        cantidad500.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad500.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad500.setText("0");
        cantidad500.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad500FocusLost(evt);
            }
        });
        cantidad500.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad500ActionPerformed(evt);
            }
        });
        cantidad500.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad500KeyTyped(evt);
            }
        });

        cantidad200.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad200.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad200.setText("0");
        cantidad200.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad200FocusLost(evt);
            }
        });
        cantidad200.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad200ActionPerformed(evt);
            }
        });
        cantidad200.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad200KeyTyped(evt);
            }
        });

        cantidad100.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad100.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad100.setText("0");
        cantidad100.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad100FocusLost(evt);
            }
        });
        cantidad100.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad100ActionPerformed(evt);
            }
        });
        cantidad100.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad100KeyTyped(evt);
            }
        });

        cantidad50.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad50.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad50.setText("0");
        cantidad50.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad50FocusLost(evt);
            }
        });
        cantidad50.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad50ActionPerformed(evt);
            }
        });
        cantidad50.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad50KeyTyped(evt);
            }
        });

        cantidad20.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad20.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad20.setText("0");
        cantidad20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad20FocusLost(evt);
            }
        });
        cantidad20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad20ActionPerformed(evt);
            }
        });
        cantidad20.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad20KeyTyped(evt);
            }
        });

        cantidad10.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad10.setText("0");
        cantidad10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad10FocusLost(evt);
            }
        });
        cantidad10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad10ActionPerformed(evt);
            }
        });
        cantidad10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad10KeyTyped(evt);
            }
        });

        cantidad5.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad5.setText("0");
        cantidad5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad5FocusLost(evt);
            }
        });
        cantidad5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad5ActionPerformed(evt);
            }
        });
        cantidad5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad5KeyTyped(evt);
            }
        });

        cantidad2.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad2.setText("0");
        cantidad2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad2FocusLost(evt);
            }
        });
        cantidad2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad2ActionPerformed(evt);
            }
        });
        cantidad2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad2KeyTyped(evt);
            }
        });

        cantidad1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad1.setText("0");
        cantidad1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad1FocusLost(evt);
            }
        });
        cantidad1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad1ActionPerformed(evt);
            }
        });
        cantidad1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad1KeyTyped(evt);
            }
        });

        cantidad50c.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        cantidad50c.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantidad50c.setText("0");
        cantidad50c.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                cantidad50cFocusLost(evt);
            }
        });
        cantidad50c.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidad50cActionPerformed(evt);
            }
        });
        cantidad50c.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidad50cKeyTyped(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Billete");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Billete");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Billete");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Billete");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Billete");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Billete");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Moneda");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Moneda");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Moneda");

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Moneda");

        jLabel15.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Moneda");

        jLabel25.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("1,000.00");

        jLabel26.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("500.00");

        jLabel27.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("200.00");

        jLabel28.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("100.00");

        jLabel29.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("50.00");

        jLabel30.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("20.00");

        jLabel31.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("10.00");

        jLabel32.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("5.00");

        jLabel33.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel33.setText("2.00");

        jLabel34.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel34.setText("1.00");

        jLabel35.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel35.setText("0.50");

        importe1000.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe1000.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe1000.setText("0.00");

        importe500.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe500.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe500.setText("0.00");

        importe200.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe200.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe200.setText("0.00");

        importe100.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe100.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe100.setText("0.00");

        importe50.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe50.setText("0.00");

        importe20.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe20.setText("0.00");

        importe10.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe10.setText("0.00");

        importe5.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe5.setText("0.00");

        importe2.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe2.setText("0.00");

        importe1.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe1.setText("0.00");

        importe50c.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        importe50c.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        importe50c.setText("0.00");

        importeTarjeta.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        importeTarjeta.setText("0.00");
        importeTarjeta.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                importeTarjetaFocusLost(evt);
            }
        });
        importeTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importeTarjetaActionPerformed(evt);
            }
        });
        importeTarjeta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                importeTarjetaKeyTyped(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Tarjeta");

        NombreC2.setBackground(new java.awt.Color(0, 0, 0));
        NombreC2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC2.setText("TOTAL:");

        TotalV.setBackground(new java.awt.Color(0, 0, 0));
        TotalV.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        TotalV.setText("0.00");

        EfectuaCorte.setBackground(new java.awt.Color(255, 255, 255));
        EfectuaCorte.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        EfectuaCorte.setText("HACER CORTE");
        EfectuaCorte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EfectuaCorteActionPerformed(evt);
            }
        });

        NombreC3.setBackground(new java.awt.Color(0, 0, 0));
        NombreC3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC3.setText("TOTAL DE VENTAS:");

        TotalSistema.setBackground(new java.awt.Color(0, 0, 0));
        TotalSistema.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        TotalSistema.setText("0.00");

        Faltante1.setBackground(new java.awt.Color(0, 0, 0));
        Faltante1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Faltante1.setText("FALTANTE:");

        Faltante.setBackground(new java.awt.Color(0, 0, 0));
        Faltante.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        Faltante.setText("0.00");

        NombreC.setBackground(new java.awt.Color(0, 0, 0));
        NombreC.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC.setText("Fecha");

        fecha.setBackground(new java.awt.Color(0, 0, 0));
        fecha.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        fecha.setText("DD/MM/AAAA");

        NombreC1.setBackground(new java.awt.Color(0, 0, 0));
        NombreC1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC1.setText("Id del Empleado: ");

        IdEmpleado.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        IdEmpleado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccionar--" }));
        IdEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IdEmpleadoActionPerformed(evt);
            }
        });

        CorteTrue.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        CorteTrue.setForeground(new java.awt.Color(255, 0, 0));
        CorteTrue.setText("Ya se ha realizado el corte del día.");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(46, 46, 46)
                            .addComponent(jLabel12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(importeTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(NombreC3)
                            .addGap(18, 18, 18)
                            .addComponent(TotalSistema, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(437, 437, 437)
                                .addComponent(NombreC2)
                                .addGap(18, 18, 18)
                                .addComponent(TotalV, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel10)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(76, 76, 76)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addGap(172, 172, 172)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel35)
                    .addComponent(jLabel34)
                    .addComponent(jLabel33)
                    .addComponent(jLabel32)
                    .addComponent(jLabel31)
                    .addComponent(jLabel30)
                    .addComponent(jLabel29)
                    .addComponent(jLabel28)
                    .addComponent(jLabel27)
                    .addComponent(jLabel26)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cantidad500, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad1000, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad200, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad100, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad20, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidad50c, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(104, 104, 104)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(importe500)
                    .addComponent(importe1000)
                    .addComponent(importe200)
                    .addComponent(importe100)
                    .addComponent(importe50)
                    .addComponent(importe20)
                    .addComponent(importe10)
                    .addComponent(importe5)
                    .addComponent(importe2)
                    .addComponent(importe1)
                    .addComponent(importe50c))
                .addGap(133, 133, 133))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CorteTrue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EfectuaCorte)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(Faltante1)
                        .addGap(18, 18, 18)
                        .addComponent(Faltante, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(41, 41, 41))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(NombreC)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fecha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(NombreC1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(IdEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NombreC)
                    .addComponent(NombreC1)
                    .addComponent(fecha)
                    .addComponent(IdEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cantidad1000, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25)
                            .addComponent(importe1000))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cantidad500, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(importe500))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(jLabel26)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cantidad200, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(importe200)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(jLabel27)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(jLabel28)
                                .addComponent(importe100))
                            .addComponent(cantidad100, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(jLabel29)
                                .addComponent(importe50))
                            .addComponent(cantidad50, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7)
                                .addComponent(jLabel30)
                                .addComponent(importe20))
                            .addComponent(cantidad20, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel8)
                                .addComponent(jLabel31)
                                .addComponent(importe10))
                            .addComponent(cantidad10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9)
                                .addComponent(jLabel32)
                                .addComponent(importe5))
                            .addComponent(cantidad5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel13)
                                .addComponent(jLabel33)
                                .addComponent(importe2))
                            .addComponent(cantidad2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel14)
                                .addComponent(jLabel34)
                                .addComponent(importe1))
                            .addComponent(cantidad1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cantidad50c, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel15)
                                .addComponent(jLabel35)
                                .addComponent(importe50c)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(importeTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(NombreC2)
                            .addComponent(TotalV))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(NombreC3)
                            .addComponent(TotalSistema))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Faltante1)
                            .addComponent(Faltante))))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EfectuaCorte)
                    .addComponent(CorteTrue))
                .addContainerGap())
        );

        cantidad1000.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ConsultaCorteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConsultaCorteActionPerformed
        ConsultarCorte consultarCorte = new ConsultarCorte();
        consultarCorte.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ConsultaCorteActionPerformed

    private void RealizaCorteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RealizaCorteActionPerformed
        // TODO add your handling code here:
        RealizarCorte realizarCorte = new RealizarCorte();
        realizarCorte.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_RealizaCorteActionPerformed

    private void RegresarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RegresarMouseClicked
        PantallaInicio abrir = new PantallaInicio();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_RegresarMouseClicked

    private void cantidad1000ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad1000ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad1000ActionPerformed

    private void cantidad1000KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad1000KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad1000KeyTyped

    private void cantidad500ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad500ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad500ActionPerformed

    private void cantidad500KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad500KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad500KeyTyped

    private void cantidad200ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad200ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad200ActionPerformed

    private void cantidad200KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad200KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad200KeyTyped

    private void cantidad100ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad100ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad100ActionPerformed

    private void cantidad100KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad100KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad100KeyTyped

    private void cantidad50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad50ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad50ActionPerformed

    private void cantidad50KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad50KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad50KeyTyped

    private void cantidad20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad20ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad20ActionPerformed

    private void cantidad20KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad20KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad20KeyTyped

    private void cantidad10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad10ActionPerformed

    private void cantidad10KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad10KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad10KeyTyped

    private void cantidad5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad5ActionPerformed

    private void cantidad5KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad5KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad5KeyTyped

    private void cantidad2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad2ActionPerformed

    private void cantidad2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad2KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad2KeyTyped

    private void cantidad1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad1ActionPerformed

    private void cantidad1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad1KeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad1KeyTyped

    private void cantidad50cActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidad50cActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantidad50cActionPerformed

    private void cantidad50cKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidad50cKeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_cantidad50cKeyTyped

    private void importeTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importeTarjetaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_importeTarjetaActionPerformed

    private void importeTarjetaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_importeTarjetaKeyTyped
        //Solo ingresa valores numericos
        var validar = evt.getKeyChar();

        if(isLetter(validar)){
            getToolkit().beep();
            evt.consume();
        }//end if
    }//GEN-LAST:event_importeTarjetaKeyTyped

    private void EfectuaCorteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EfectuaCorteActionPerformed
        try{
            //Panel de Advertencia
            int seleccion = JOptionPane.showOptionDialog(this,"¿Esta seguro de que desea realizar el corte? \n (Dicha opción no se puede deshacer).",
            "ADVERTENCIA",JOptionPane.WARNING_MESSAGE,
            JOptionPane.WARNING_MESSAGE,null,// null para icono por defecto.
            new Object[] { "Aceptar", "Cancelar"},"Aceptar");
            //int hacerCorte = JOptionPane.showConfirmDialog(null, "¿Esta seguro de que desea realizar el corte? \n (Dicha opción no se puede deshacer).", "ADVERTENCIA", WIDTH, HEIGHT);           
            
            //Agregar valores a la tabla Venta
            if(seleccion == 0){
                String SSQL = "INSERT INTO Cortes (Fecha, Total, idEmpleado) "
                + "VALUES (?, ?, ?)";
                String Fecha = this.fecha.getText();
                String Emp = this.IdEmpleado.getModel().getSelectedItem().toString();
                String aux = "";
                int idEmp;
                String Total = this.TotalV.getText();

                for(int i = 0; i<Emp.length(); i++){
                    if(Emp.charAt(i) != ' '){
                        aux += Emp.charAt(i);
                    }
                    else
                        break;
                }

                idEmp = Integer.parseInt(aux);

                ps = (PreparedStatement) con.prepareStatement(SSQL);
                ps.setString(1, Fecha);
                ps.setString(2, Total);
                ps.setInt(3, idEmp);

                int res = ps.executeUpdate();
                
                if(res > 0){
                    javax.swing.JOptionPane.showMessageDialog(this, "El Corte de caja se realizo exitosamente", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                    System.out.println("Los valores han sido agregados a la base de datos ");
                }
                else{
                   JOptionPane.showMessageDialog(null, "Error al realizar corte de caja"); 
                } 
            }

            
        } catch (SQLException ex) {
            Logger.getLogger(RealizarCorte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_EfectuaCorteActionPerformed

    private void IdEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IdEmpleadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IdEmpleadoActionPerformed

    private void cantidad1000FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad1000FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad1000.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad1000.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 1000;
                this.importe1000.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad1000.setText("0");
                cantidad = 0;
                this.importe1000.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad1000.setText("0");
            cantidad = 0;
            this.importe1000.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad1000FocusLost

    private void cantidad500FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad500FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad500.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad500.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 500;
                this.importe500.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad500.setText("0");
                cantidad = 0;
                this.importe500.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad500.setText("0");
            cantidad = 0;
            this.importe500.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad500FocusLost

    private void cantidad200FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad200FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad200.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad200.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 200;
                this.importe200.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad200.setText("0");
                cantidad = 0;
                this.importe200.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad200.setText("0");
            cantidad = 0;
            this.importe200.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad200FocusLost

    private void cantidad100FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad100FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad100.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad100.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 100;
                this.importe100.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad100.setText("0");
                cantidad = 0;
                this.importe100.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad100.setText("0");
            cantidad = 0;
            this.importe100.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad100FocusLost

    private void cantidad50FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad50FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad50.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad50.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 50;
                this.importe50.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad50.setText("0");
                cantidad = 0;
                this.importe50.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad50.setText("0");
            cantidad = 0;
            this.importe50.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad50FocusLost

    private void cantidad20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad20FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad20.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad20.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 20;
                this.importe20.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad20.setText("0");
                cantidad = 0;
                this.importe20.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad20.setText("0");
            cantidad = 0;
            this.importe20.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad20FocusLost

    private void cantidad10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad10FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad10.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad10.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 10;
                this.importe10.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad10.setText("0");
                cantidad = 0;
                this.importe10.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad10.setText("0");
            cantidad = 0;
            this.importe10.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad10FocusLost

    private void cantidad5FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad5FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad5.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad5.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 5;
                this.importe5.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad5.setText("0");
                cantidad = 0;
                this.importe5.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad5.setText("0");
            cantidad = 0;
            this.importe5.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad5FocusLost

    private void cantidad2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad2FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad2.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad2.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 2;
                this.importe2.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad2.setText("0");
                cantidad = 0;
                this.importe2.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad2.setText("0");
            cantidad = 0;
            this.importe2.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad2FocusLost

    private void cantidad1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad1FocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad1.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad1.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 1;
                this.importe1.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad1.setText("0");
                cantidad = 0;
                this.importe1.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad1.setText("0");
            cantidad = 0;
            this.importe1.setText("0.00");
        }
        calculaTotalCorte();
    }//GEN-LAST:event_cantidad1FocusLost

    private void cantidad50cFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cantidad50cFocusLost
        // TODO add your handling code here:
        int cantidad = 0;
        double subtotal = 0;
        String aux = this.cantidad50c.getText();
        
        if(!"".equals(aux)){
            cantidad = Integer.parseInt(this.cantidad50c.getText());
            if(cantidad >= 0){
                subtotal = cantidad * 0.50;
                this.importe50c.setText(String.valueOf(String.format("%.2f", subtotal)));
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.cantidad50c.setText("0");
                cantidad = 0;
                this.importe50c.setText("0.00");
            }
        }
        else if("".equals(aux)){
            this.cantidad50c.setText("0");
            cantidad = 0;
            this.importe50c.setText("0.00");
        }
        calculaTotalCorte();
        
    }//GEN-LAST:event_cantidad50cFocusLost

    private void importeTarjetaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_importeTarjetaFocusLost
        // TODO add your handling code here:
        double importe = 0;
        double subtotal = 0;
        String aux = this.importeTarjeta.getText();
        
        if(!"".equals(aux)){
            if(Double.parseDouble(this.importeTarjeta.getText()) >= 0){
                importe = Double.parseDouble(this.importeTarjeta.getText()); 
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
                this.importeTarjeta.setText("0.00");
            }       
        }
        else if("".equals(aux)){
            this.importeTarjeta.setText("0.00");
        }
        calculaTotalCorte();
        
    }//GEN-LAST:event_importeTarjetaFocusLost

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RealizarCorte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new RealizarCorte().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ConsultaCorte;
    private javax.swing.JLabel CorteTrue;
    private javax.swing.JButton EfectuaCorte;
    private javax.swing.JLabel Faltante;
    private javax.swing.JLabel Faltante1;
    private javax.swing.JComboBox<String> IdEmpleado;
    private javax.swing.JLabel Label1;
    private javax.swing.JLabel Label2;
    private javax.swing.JLabel Label3;
    private javax.swing.JLabel Label4;
    private javax.swing.JLabel Label5;
    private javax.swing.JLabel Label6;
    private javax.swing.JLabel NombreC;
    private javax.swing.JLabel NombreC1;
    private javax.swing.JLabel NombreC2;
    private javax.swing.JLabel NombreC3;
    private javax.swing.JButton RealizaCorte;
    private javax.swing.JLabel Regresar;
    private javax.swing.JLabel TotalSistema;
    private javax.swing.JLabel TotalV;
    private javax.swing.JTextField cantidad1;
    private javax.swing.JTextField cantidad10;
    private javax.swing.JTextField cantidad100;
    private javax.swing.JTextField cantidad1000;
    private javax.swing.JTextField cantidad2;
    private javax.swing.JTextField cantidad20;
    private javax.swing.JTextField cantidad200;
    private javax.swing.JTextField cantidad5;
    private javax.swing.JTextField cantidad50;
    private javax.swing.JTextField cantidad500;
    private javax.swing.JTextField cantidad50c;
    private javax.swing.JLabel fecha;
    private javax.swing.JLabel importe1;
    private javax.swing.JLabel importe10;
    private javax.swing.JLabel importe100;
    private javax.swing.JLabel importe1000;
    private javax.swing.JLabel importe2;
    private javax.swing.JLabel importe20;
    private javax.swing.JLabel importe200;
    private javax.swing.JLabel importe5;
    private javax.swing.JLabel importe50;
    private javax.swing.JLabel importe500;
    private javax.swing.JLabel importe50c;
    private javax.swing.JTextField importeTarjeta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables
}
