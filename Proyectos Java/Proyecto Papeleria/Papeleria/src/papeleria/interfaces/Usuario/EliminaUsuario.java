/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria.interfaces.Usuario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import papeleria.interfaces.Cliente.EliminaCliente;
import papeleria.interfaces.PantallaInicio;


public final class EliminaUsuario extends javax.swing.JFrame {

    /**
     * Creates new form EliminaUsuario
     */
    
    Connection con = null;
    Statement stmt = null;
    
    String titulos[] = {"ID", "Usuario", "Rol", "Nombre"};
    String fila[] = new String[4];
    DefaultTableModel modelo;
    
    public void Consulta(){
        try {

            String url = "jdbc:mysql://localhost:3306/Papeleria";
            String usuario = "root";
            String contraseña = "";
            String elim = "";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Usuarios");

            modelo = new DefaultTableModel(null, titulos);
            
            String idDueEmp = "";
            String NomDueEmp = "";
            

            while (rs.next()) {
                idDueEmp = "";
                fila[0] = rs.getString("IdRol");
                fila[1] = rs.getString("Usuario");
                fila[2] = rs.getString("Rol");
                
                if("Empleado".equals(rs.getString("Rol")))
                    idDueEmp = rs.getString("idEmpleado");
                
                else
                    idDueEmp = rs.getString("idDuenio");
                    
                
                NomDueEmp = obtenerNomDueEmp(idDueEmp,rs.getString("Rol"));
                
                fila[3] = NomDueEmp;

                modelo.addRow(fila);
            }
            Consultas.setModel(modelo);
            
            
            TableColumn id = Consultas.getColumn("ID");
            id.setMaxWidth(100);
            TableColumn user = Consultas.getColumn("Usuario");
            user.setMaxWidth(250);
            TableColumn rol = Consultas.getColumn("Rol");
            rol.setMaxWidth(200);
            TableColumn nom = Consultas.getColumn("Nombre");
            nom.setMaxWidth(300);               

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }
    
    public String obtenerNomDueEmp (String idDueEmp, String Rol){
        String Nombre = "";
        try{
            stmt = con.createStatement();
            if(Rol.equals("Empleado")){
                ResultSet rs = stmt.executeQuery("select * from Empleado WHERE IdEmpleado = " + idDueEmp);

                while (rs.next()) {
                    Nombre = rs.getString("IdEmpleado") + " - " + rs.getString("Nombre");
                }                    
            }
            else{
                ResultSet rs = stmt.executeQuery("select * from Duenio WHERE IdDuenio = " + idDueEmp);

                while (rs.next()) {
                    Nombre = rs.getString("IdDuenio") + " - " + rs.getString("Nombre");
                }                
            }
        
        } catch (SQLException ex) {
            Logger.getLogger(EliminaUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Nombre;
    }
    
    public void Elimina(){  
        try {
            int filaSeleccionada = Consultas.getSelectedRow();
            String SQLelim = "DELETE FROM Usuarios WHERE idRol = " + Consultas.getValueAt(filaSeleccionada, 0);
            System.out.println(SQLelim);
            Statement st= con.createStatement();
            
            int n = st.executeUpdate(SQLelim);
            
            if(n >= 0){
                javax.swing.JOptionPane.showMessageDialog(this, "Usuario Eliminado Correctamente", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            } 
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(this, "Error al eliminar usuario", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            Logger.getLogger(EliminaCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        catch(IndexOutOfBoundsException e){
            javax.swing.JOptionPane.showMessageDialog(this, "Selecciona un elemento de la Tabla.", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
            Logger.getLogger(EliminaCliente.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
  
    public EliminaUsuario() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        Consulta();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        CambiaContra = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        NuevoUsu = new javax.swing.JButton();
        EliminarUsu = new javax.swing.JButton();
        Regresar = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Consultas = new javax.swing.JTable();
        Consulta = new javax.swing.JLabel();
        ConsultaCli = new javax.swing.JTextField();
        Buscar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        EliminarCli = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(102, 153, 255));

        CambiaContra.setBackground(new java.awt.Color(153, 0, 0));
        CambiaContra.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        CambiaContra.setForeground(new java.awt.Color(255, 255, 255));
        CambiaContra.setText("Cambia Contraseña");
        CambiaContra.setBorder(null);
        CambiaContra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CambiaContraActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("USUARIOS");

        NuevoUsu.setBackground(new java.awt.Color(153, 0, 0));
        NuevoUsu.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        NuevoUsu.setForeground(new java.awt.Color(255, 255, 255));
        NuevoUsu.setText("Nuevo Usuario");
        NuevoUsu.setBorder(null);
        NuevoUsu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NuevoUsuActionPerformed(evt);
            }
        });

        EliminarUsu.setBackground(new java.awt.Color(153, 0, 0));
        EliminarUsu.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        EliminarUsu.setForeground(new java.awt.Color(255, 255, 255));
        EliminarUsu.setText("Eliminar");
        EliminarUsu.setBorder(null);
        EliminarUsu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarUsuActionPerformed(evt);
            }
        });

        Regresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Regresar.png"))); // NOI18N
        Regresar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                RegresarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CambiaContra, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                    .addComponent(EliminarUsu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(NuevoUsu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Regresar)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(NuevoUsu, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(CambiaContra, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(EliminarUsu, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Regresar)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setPreferredSize(new java.awt.Dimension(700, 600));

        Consultas.setBackground(new java.awt.Color(102, 255, 102));
        Consultas.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        Consultas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id", "Usuario", "Rol", "Nombre"
            }
        ));
        Consultas.setFocusable(false);
        Consultas.setGridColor(new java.awt.Color(0, 0, 0));
        Consultas.setSelectionBackground(new java.awt.Color(225, 225, 225));
        Consultas.setSelectionForeground(new java.awt.Color(0, 0, 0));
        Consultas.setShowVerticalLines(false);
        jScrollPane1.setViewportView(Consultas);

        Consulta.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Consulta.setForeground(new java.awt.Color(255, 255, 255));
        Consulta.setText("Ingresa el id o nombre del usuario:");

        ConsultaCli.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        ConsultaCli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConsultaCliActionPerformed(evt);
            }
        });

        Buscar.setBackground(new java.awt.Color(255, 255, 255));
        Buscar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Buscar.setText("Buscar");
        Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Eliminar Usuarios");

        EliminarCli.setBackground(new java.awt.Color(255, 255, 255));
        EliminarCli.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        EliminarCli.setText("E L I M I N A R");
        EliminarCli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarCliActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 77, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(Consulta)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ConsultaCli, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Buscar)
                                .addGap(122, 122, 122))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(EliminarCli, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(301, 301, 301))))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ConsultaCli)
                    .addComponent(Consulta)
                    .addComponent(Buscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(EliminarCli)
                .addGap(38, 38, 38))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 823, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 648, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CambiaContraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CambiaContraActionPerformed
        CambiarContra abrir = new CambiarContra();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_CambiaContraActionPerformed

    private void NuevoUsuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NuevoUsuActionPerformed
        NuevoUsuario abrir = new NuevoUsuario();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_NuevoUsuActionPerformed

    private void EliminarUsuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarUsuActionPerformed
        EliminaUsuario abrir = new EliminaUsuario();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_EliminarUsuActionPerformed

    private void RegresarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RegresarMouseClicked
        PantallaInicio abrir = new PantallaInicio();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_RegresarMouseClicked

    private void ConsultaCliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConsultaCliActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ConsultaCliActionPerformed

    private void BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarActionPerformed
        try {
            stmt = con.createStatement();
            int idB;
            String nombreB = ConsultaCli.getText();
            ResultSet rs;

            if("".equals(nombreB)){
                rs = stmt.executeQuery("select * from Usuarios");
            }

            else if(nombreB.charAt(0) >= 48 && nombreB.charAt(0) <= 57){
                idB = Integer.parseInt(nombreB);
                rs = stmt.executeQuery("select * from Usuarios WHERE idRol = " + idB);
            }
            else{
                rs = stmt.executeQuery("select * from Usuarios WHERE Usuario  LIKE '" + "%" + nombreB + "%" + "'");
            }

            modelo = new DefaultTableModel(null, titulos);
            String idDueEmp = "";
            String NomDueEmp = "";            

            while (rs.next()) {
                idDueEmp = "";
                fila[0] = rs.getString("IdRol");
                fila[1] = rs.getString("Usuario");
                fila[2] = rs.getString("Rol");
                
                if("Empleado".equals(rs.getString("Rol")))
                    idDueEmp = rs.getString("idEmpleado");
                
                else
                    idDueEmp = rs.getString("idDuenio");
                    
                
                NomDueEmp = obtenerNomDueEmp(idDueEmp,rs.getString("Rol"));
                
                fila[3] = NomDueEmp;

                modelo.addRow(fila);
            }
            Consultas.setModel(modelo);
            
            
            TableColumn id = Consultas.getColumn("ID");
            id.setMaxWidth(100);
            TableColumn user = Consultas.getColumn("Usuario");
            user.setMaxWidth(250);
            TableColumn rol = Consultas.getColumn("Rol");
            rol.setMaxWidth(200);
            TableColumn nom = Consultas.getColumn("Nombre");
            nom.setMaxWidth(300);               

        } catch (NumberFormatException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }//GEN-LAST:event_BuscarActionPerformed

    private void EliminarCliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarCliActionPerformed
        int seleccion = JOptionPane.showOptionDialog(this,"¿Esta seguro de que eliminar el Usuario?",
            "ADVERTENCIA",JOptionPane.WARNING_MESSAGE,
            JOptionPane.WARNING_MESSAGE,null,// null para icono por defecto.
            new Object[] { "Aceptar", "Cancelar"},"Aceptar");

        if(seleccion == 0){
            Elimina();
            Consulta();
        }
    }//GEN-LAST:event_EliminarCliActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EliminaUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EliminaUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EliminaUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EliminaUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EliminaUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Buscar;
    private javax.swing.JButton CambiaContra;
    private javax.swing.JLabel Consulta;
    private javax.swing.JTextField ConsultaCli;
    private javax.swing.JTable Consultas;
    private javax.swing.JButton EliminarCli;
    private javax.swing.JButton EliminarUsu;
    private javax.swing.JButton NuevoUsu;
    private javax.swing.JLabel Regresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
