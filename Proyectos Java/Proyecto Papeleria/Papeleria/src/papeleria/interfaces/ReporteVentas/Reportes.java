/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria.interfaces.ReporteVentas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import papeleria.interfaces.PantallaInicio;


public class Reportes extends javax.swing.JFrame {

    /**
     * Creates new form Reportes
     */
    
    Connection con = null;
    Statement stmt = null;
    
    String titulos[] = {"Fecha", "Ventas", "Crecimiento", "% frente a la venta anterior"};
    String fila[] = new String[6];
    DefaultTableModel modelo;    
    
    public Reportes() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        Consulta();
        actualizarTotal();
    }
    
    public void Consulta(){
        try {

            String url = "jdbc:mysql://localhost:3306/Papeleria";
            String usuario = "root";
            String contraseña = "";
            double totalSist = 0;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Cortes");
            String Empleado = "";

            modelo = new DefaultTableModel(null, titulos);
            
            ArrayList<Double> Venta = new ArrayList<Double>();

            for (int i = 0; rs.next(); i++) {
                fila[0] = rs.getString("Fecha");               
                fila[1] = String.format("%.2f",obtenerTotalVenta(rs.getString("Fecha")));
                
                Venta.add(Double.parseDouble(fila[1]));
                if(i == 0){
                    fila[2] = fila[1];                    
                }
                
                else{
                    fila[2] = String.format("%.2f", Double.parseDouble(fila[1]) - Venta.get(i-1));
                }

                fila[3] = String.format("%.2f", Double.parseDouble(fila[2])/Double.parseDouble(fila[1]) * 100);
                System.out.println(i);
                modelo.addRow(fila);
            }
            Consultas.setModel(modelo);
            TableColumn fec = Consultas.getColumn("Fecha");
            fec.setMaxWidth(200);
            TableColumn imp = Consultas.getColumn("Ventas");
            imp.setMaxWidth(250);
            TableColumn cre = Consultas.getColumn("Crecimiento");
            cre.setMaxWidth(250);            
            TableColumn porcent = Consultas.getColumn("% frente a la venta anterior");
            porcent.setMaxWidth(200);

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }
    
    public String obtenerEmpleado (String idEmp){
        String Empleado = "";
        try{
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Empleado WHERE IdEmpleado = " + idEmp);

            while (rs.next()) {
                Empleado = rs.getString("IdEmpleado") + " - " + rs.getString("Nombre");
            }            
        } catch (SQLException ex) {
            Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        
        return Empleado;
    }
    
    
    public double obtenerTotalVenta(String Fecha){
        double Total = 0;
        
            try {
                int idVenta = 0;
                stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("select idVenta from Venta WHERE Fecha = '" + Fecha + "'");

                //System.out.println(Fecha);
                while(rs.next()){
                    int idProducto = 0;
                    int cantidad = 0;
                    idVenta = rs.getInt("idVenta");
                    stmt = con.createStatement();
                    ResultSet rs1 = stmt.executeQuery("select * from VenPro WHERE idVenta = " + idVenta); 
                    while(rs1.next()){
                        double precio = 0;
                        idProducto = rs1.getInt("IdProducto");
                        cantidad = rs1.getInt("Cantidad");
                        stmt = con.createStatement();
                        ResultSet rs2 = stmt.executeQuery("select Precio from Producto WHERE idProducto = " + idProducto);
                        while(rs2.next()){
                            precio = rs2.getDouble("Precio");
                        }
                        Total += precio*cantidad;
                        }
                    }
                    //System.out.println(Total);


                } catch (SQLException ex) {
                    Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
                }
        
                return Total;
    }
    
    public void actualizarTotal(){
        float total = 0, p;
        float crecimiento = 0, q;
        float porcentaje = 0, r;
        
        String tot1 = "";
        String crec1 = "";
        String porc1 = "";
        
        
        if(this.Consultas.getRowCount() == 0){
            this.TotalV.setText("-----");
            this.Crecimiento1.setText("-----");
            this.Porcentaje1.setText("-----");
        }
        
        else if(Consultas.getRowCount()>0){
            for(int i = 0; i< Consultas.getRowCount(); i++){
                p = Float.parseFloat(Consultas.getValueAt(i, 1).toString());
                total += p;
                
                q = Float.parseFloat(Consultas.getValueAt(i, 2).toString());
                crecimiento += q;

                r = Float.parseFloat(Consultas.getValueAt(i, 3).toString());
                porcentaje += r;
            }
            
            tot1 = "$" + String.format("%.2f", total);
            crec1 = "$" + String.format("%.2f", crecimiento);
            porc1 = String.format("%.2f", porcentaje) + "%";
            
            this.TotalV.setText(tot1);
            this.Crecimiento1.setText(crec1);
            this.Porcentaje1.setText(porc1);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Consultas = new javax.swing.JTable();
        Consulta = new javax.swing.JLabel();
        Buscar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        Consulta1 = new javax.swing.JLabel();
        Nombre1 = new javax.swing.JTextField();
        Nombre2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        NombreC2 = new javax.swing.JLabel();
        TotalV = new javax.swing.JLabel();
        Crecimiento1 = new javax.swing.JLabel();
        Porcentaje1 = new javax.swing.JLabel();
        Regresar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 153, 102));

        Consultas.setAutoCreateRowSorter(true);
        Consultas.setBackground(new java.awt.Color(102, 255, 102));
        Consultas.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        Consultas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Fecha", "Ventas", "Crecimiento", "% frente al día anterior"
            }
        ));
        Consultas.setFocusable(false);
        Consultas.setGridColor(new java.awt.Color(0, 0, 0));
        Consultas.setSelectionBackground(new java.awt.Color(225, 225, 225));
        Consultas.setSelectionForeground(new java.awt.Color(0, 0, 0));
        Consultas.setShowVerticalLines(false);
        jScrollPane1.setViewportView(Consultas);

        Consulta.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Consulta.setForeground(new java.awt.Color(255, 255, 255));
        Consulta.setText("De:");

        Buscar.setBackground(new java.awt.Color(255, 255, 255));
        Buscar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Buscar.setText("Buscar");
        Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Reporte de Ventas");

        Consulta1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Consulta1.setForeground(new java.awt.Color(255, 255, 255));
        Consulta1.setText("A:");

        Nombre1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        Nombre1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nombre1ActionPerformed(evt);
            }
        });

        Nombre2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        Nombre2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nombre2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("*En formato AAAA-MM-DD");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("*En formato AAAA-MM-DD");

        NombreC2.setBackground(new java.awt.Color(0, 0, 0));
        NombreC2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC2.setText("TOTAL:");

        TotalV.setBackground(new java.awt.Color(0, 0, 0));
        TotalV.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        TotalV.setText("-----");

        Crecimiento1.setBackground(new java.awt.Color(0, 0, 0));
        Crecimiento1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        Crecimiento1.setText("-----");

        Porcentaje1.setBackground(new java.awt.Color(0, 0, 0));
        Porcentaje1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        Porcentaje1.setText("-----");

        Regresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Regresar.png"))); // NOI18N
        Regresar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                RegresarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 976, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 880, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(Consulta)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(Nombre2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(171, 171, 171)
                                        .addComponent(Consulta1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel1)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel2)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(Nombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(Buscar)))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(NombreC2)
                .addGap(143, 143, 143)
                .addComponent(TotalV, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(127, 127, 127)
                .addComponent(Crecimiento1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(117, 117, 117)
                .addComponent(Porcentaje1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Regresar))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Consulta)
                    .addComponent(Buscar)
                    .addComponent(Consulta1)
                    .addComponent(Nombre1)
                    .addComponent(Nombre2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(NombreC2)
                        .addComponent(TotalV)
                        .addComponent(Crecimiento1)
                        .addComponent(Porcentaje1))
                    .addComponent(Regresar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarActionPerformed
        try {
            String Fecha1 = this.Nombre2.getText();
            String Fecha2 = this.Nombre1.getText();
            
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Cortes WHERE fecha BETWEEN '" + Fecha1 + "' AND '" + Fecha2 + "'");
            String Empleado = "";

            modelo = new DefaultTableModel(null, titulos);
            
            ArrayList<Double> Venta = new ArrayList<Double>();

            for (int i = 0; rs.next(); i++) {
                fila[0] = rs.getString("Fecha");               
                fila[1] = String.format("%.2f",obtenerTotalVenta(rs.getString("Fecha")));
                
                Venta.add(Double.parseDouble(fila[1]));
                if(i == 0){
                    fila[2] = fila[1];                    
                }
                
                else{
                    fila[2] = String.format("%.2f", Double.parseDouble(fila[1]) - Venta.get(i-1));
                }

                fila[3] = String.format("%.2f", Double.parseDouble(fila[2])/Double.parseDouble(fila[1]) * 100);
                System.out.println(i);
                modelo.addRow(fila);
            }
            Consultas.setModel(modelo);
            TableColumn fec = Consultas.getColumn("Fecha");
            fec.setMaxWidth(200);
            TableColumn imp = Consultas.getColumn("Ventas");
            imp.setMaxWidth(250);
            TableColumn cre = Consultas.getColumn("Crecimiento");
            cre.setMaxWidth(250);            
            TableColumn porcent = Consultas.getColumn("% frente a la venta anterior");
            porcent.setMaxWidth(200);
            
            actualizarTotal();

        } catch (SQLException ex) {
            Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }//GEN-LAST:event_BuscarActionPerformed

    private void Nombre1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nombre1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Nombre1ActionPerformed

    private void Nombre2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nombre2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Nombre2ActionPerformed

    private void RegresarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RegresarMouseClicked
        PantallaInicio abrir = new PantallaInicio();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_RegresarMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Reportes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Buscar;
    private javax.swing.JLabel Consulta;
    private javax.swing.JLabel Consulta1;
    private javax.swing.JTable Consultas;
    private javax.swing.JLabel Crecimiento1;
    private javax.swing.JTextField Nombre1;
    private javax.swing.JTextField Nombre2;
    private javax.swing.JLabel NombreC2;
    private javax.swing.JLabel Porcentaje1;
    private javax.swing.JLabel Regresar;
    private javax.swing.JLabel TotalV;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
