-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-09-2021 a las 23:43:28
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `papeleria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(5) NOT NULL,
  `Nombre` varchar(35) NOT NULL,
  `AP` varchar(35) NOT NULL,
  `AM` varchar(35) NOT NULL,
  `Calle` varchar(35) NOT NULL,
  `Numero` varchar(35) NOT NULL,
  `Colonia` varchar(35) NOT NULL,
  `Municipio` varchar(35) NOT NULL,
  `Estado` varchar(35) NOT NULL,
  `CP` varchar(35) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Telefono` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `Nombre`, `AP`, `AM`, `Calle`, `Numero`, `Colonia`, `Municipio`, `Estado`, `CP`, `Correo`, `Telefono`) VALUES
(1, 'Luis Francisco', 'Jimenez', 'Vigueras', 'La Venta', '5', 'El Carmen', 'Tzompantepec', 'Tlaxcala', '90491', 'franciscojimenez0716@gmail.com', 2411344270),
(3, 'Zabdiel', 'Carro', 'Cuellar', 'gfds', '0', 'jhgff', 'Panotla', 'Tlaxcala', '90105', 'jhgfd', 0),
(4, 'Pepe', 'hfgds', 'gfds', 'gfds', '0', 'gdfsag', 'gfdsea', 'Puebla', '90491', 'gfds', 0),
(5, 'gdfrsde', 'fdrs', 'fdrs', 'fdrs', '0', 'desfrd', 'rsda', 'Tlaxcala', '90105', 'gdfs', 0),
(9, 'Andrea', 'Martínez', 'Hernández', 'asdf', '1', 'QASDFGH', 'RDRFGH', 'Veracruz', '74123', 'drfgth', 2411452252),
(10, 'Miriam', 'Paredes', 'Atonal', 'dfghjk', '14', 'gfds', 'gfdcs', 'Tlaxcala', '90491', 'gfds', 145);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cortes`
--

CREATE TABLE `cortes` (
  `IdCorte` int(5) NOT NULL,
  `Fecha` date NOT NULL,
  `Total` decimal(10,2) NOT NULL,
  `IdEmpleado` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cortes`
--

INSERT INTO `cortes` (`IdCorte`, `Fecha`, `Total`, `IdEmpleado`) VALUES
(3, '2020-11-22', '120.48', 1),
(7, '2020-11-23', '11.98', 1),
(8, '2020-11-27', '111.98', 2),
(9, '2020-11-29', '140.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `duenio`
--

CREATE TABLE `duenio` (
  `IdDuenio` int(5) NOT NULL,
  `Nombre` varchar(35) NOT NULL,
  `AP` varchar(35) NOT NULL,
  `AM` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `duenio`
--

INSERT INTO `duenio` (`IdDuenio`, `Nombre`, `AP`, `AM`) VALUES
(1, 'Fatima', 'Jimenez', 'Juarez'),
(2, 'Luis', 'Jimenez', 'Juarez'),
(3, 'Juan', 'Jimenez', 'Juarez'),
(4, 'José Eduardo', 'Zamora', 'Quechol');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(5) NOT NULL,
  `Nombre` varchar(35) NOT NULL,
  `AP` varchar(35) NOT NULL,
  `AM` varchar(35) NOT NULL,
  `Calle` varchar(50) NOT NULL,
  `Numero` int(11) NOT NULL,
  `Colonia` varchar(50) NOT NULL,
  `Municipio` varchar(50) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `CP` int(5) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Telefono` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `Nombre`, `AP`, `AM`, `Calle`, `Numero`, `Colonia`, `Municipio`, `Estado`, `CP`, `Correo`, `Telefono`) VALUES
(1, 'Luis Francisco', 'Jimenez', 'Vigueras', 'La Venta', 0, 'El Carmen', 'Tzompantepec', 'Tlaxcala', 90491, 'franciscojimenez0716@gmail.com', 2411384500),
(2, 'Luis', 'Jimenez', 'Vigueras', 'La Venta', 0, 'El Carmen', 'Tzompantepec', 'Tlaxcala', 90491, 'franciscojimenez0716@gmail.com', 2411384500),
(3, 'Brandon', 'Pilotzi', 'Bautista', 'Reforma', 29, 'San Rafael Tepatlaxco', 'Chiautempan', 'Tlaxcala', 90821, 'Brandon_Bautista2017@hotmail.com', 2213449117),
(4, 'José Eduardo', 'Zamora', 'Quechol', 'Dolores', 35, 'Texcacoac', 'Chiautempan', 'Tlaxcala', 90806, 'eduml02ax@gmail.com', 2461071153);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idFactura` int(5) NOT NULL,
  `idVenta` int(5) NOT NULL,
  `idCliente` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idFactura`, `idVenta`, `idCliente`) VALUES
(1, 1, 1),
(2, 1, 3),
(3, 1, 1),
(4, 1, 1),
(5, 6, 1),
(6, 6, 1),
(7, 6, 1),
(8, 6, 3),
(9, 6, 1),
(10, 6, 1),
(11, 6, 1),
(12, 1, 3),
(13, 6, 1),
(14, 6, 1),
(15, 6, 9),
(16, 1, 9),
(17, 1, 3),
(18, 1, 3),
(19, 1, 1),
(20, 1, 1),
(21, 1, 1),
(22, 1, 1),
(23, 6, 3),
(24, 6, 9),
(25, 6, 3),
(26, 6, 1),
(27, 6, 1),
(28, 6, 1),
(29, 1, 1),
(30, 1, 1),
(31, 1, 1),
(32, 1, 3),
(33, 1, 3),
(34, 6, 3),
(35, 6, 9),
(36, 6, 3),
(37, 6, 9),
(38, 6, 9),
(39, 6, 9),
(40, 6, 9),
(41, 6, 9),
(42, 6, 9),
(43, 6, 9),
(44, 6, 9),
(45, 6, 9),
(46, 6, 9),
(47, 6, 9),
(48, 6, 9),
(49, 6, 9),
(50, 6, 9),
(51, 6, 9),
(52, 6, 9),
(53, 6, 10),
(54, 6, 9),
(55, 1, 3),
(56, 20, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `IdInventario` int(5) NOT NULL,
  `Fecha` date NOT NULL,
  `IdDuenio` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`IdInventario`, `Fecha`, `IdDuenio`) VALUES
(26, '2020-11-24', 1),
(27, '2020-11-24', 1),
(28, '2020-11-24', 2),
(29, '2020-11-24', 1),
(30, '2020-11-24', 1),
(31, '2020-11-24', 1),
(32, '2020-11-24', 2),
(33, '2020-11-24', 2),
(34, '2020-11-24', 2),
(36, '2020-11-24', 1),
(37, '2020-11-24', 3),
(38, '2020-11-24', 2),
(39, '2020-11-24', 1),
(40, '2020-11-24', 1),
(41, '2020-11-24', 2),
(42, '2020-11-24', 3),
(43, '2020-11-24', 2),
(44, '2020-11-24', 2),
(45, '2020-11-24', 3),
(46, '2020-11-24', 1),
(47, '2020-11-24', 3),
(48, '2020-11-24', 2),
(49, '2020-11-24', 2),
(50, '2020-11-24', 1),
(51, '2020-11-24', 3),
(52, '2020-11-24', 2),
(53, '2020-11-25', 3),
(54, '2020-11-25', 2),
(55, '2020-11-25', 1),
(56, '2020-11-25', 1),
(57, '2020-11-27', 2),
(58, '2020-11-27', 3),
(59, '2020-11-27', 2),
(60, '2020-11-27', 2),
(61, '2020-11-27', 2),
(62, '2020-11-27', 2),
(63, '2020-11-27', 2),
(64, '2020-11-27', 2),
(65, '2020-11-27', 1),
(66, '2020-11-27', 2),
(67, '2020-11-29', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invpro`
--

CREATE TABLE `invpro` (
  `IdProducto` int(5) NOT NULL,
  `IdInventario` int(5) NOT NULL,
  `ExistenciaReal` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `invpro`
--

INSERT INTO `invpro` (`IdProducto`, `IdInventario`, `ExistenciaReal`) VALUES
(1, 26, 33),
(1, 28, 35),
(1, 32, 0),
(1, 33, 0),
(1, 34, 0),
(1, 37, 0),
(1, 42, 0),
(1, 66, 33),
(1, 67, 70),
(3, 26, 34),
(3, 28, 36),
(3, 32, 0),
(3, 33, 0),
(3, 34, 0),
(3, 37, 0),
(3, 42, 0),
(3, 66, 34),
(3, 67, 49),
(7, 26, 37),
(7, 66, 38),
(7, 67, 38);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `idPedido` int(5) NOT NULL,
  `Fecha` date NOT NULL,
  `idProveedor` int(5) NOT NULL,
  `IdEmpleado` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`idPedido`, `Fecha`, `idProveedor`, `IdEmpleado`) VALUES
(1, '2020-11-16', 1, 1),
(5, '2020-11-27', 1, 2),
(6, '2020-11-27', 1, 1),
(8, '2020-11-29', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(5) NOT NULL,
  `Nombre` varchar(35) NOT NULL,
  `Descripcion` text NOT NULL,
  `Precio` decimal(10,2) NOT NULL,
  `Stock` int(5) NOT NULL,
  `precioProveedor` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `Nombre`, `Descripcion`, `Precio`, `Stock`, `precioProveedor`) VALUES
(1, 'Libreta', 'Profesional cuadro 7 mm', '10.00', 69, '15.00'),
(3, 'Lapiz', 'Pues para escribir jajaja', '5.99', 49, '3.50'),
(7, 'Calculadora', 'Calcula la distancia a la que me quiere lejos', '60.00', 36, '40.50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proped`
--

CREATE TABLE `proped` (
  `idPedido` int(5) NOT NULL,
  `idProducto` int(5) NOT NULL,
  `Cantidad` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proped`
--

INSERT INTO `proped` (`idPedido`, `idProducto`, `Cantidad`) VALUES
(1, 1, 6),
(1, 3, 7),
(5, 1, 1),
(5, 7, 12),
(6, 1, 15),
(6, 3, 15),
(8, 1, 25);

--
-- Disparadores `proped`
--
DELIMITER $$
CREATE TRIGGER `aumentoProducto` AFTER INSERT ON `proped` FOR EACH ROW begin
declare cantProducto int (5);
set cantProducto = (select Cantidad from ProPed where idProducto=new.idProducto and idPedido=new.idPedido);
update Producto set Stock=Stock+cantProducto where idProducto=new.idProducto;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `descProducto2` BEFORE DELETE ON `proped` FOR EACH ROW begin
declare cantProducto int (5);
set cantProducto = (select Cantidad from ProPed where idProducto=old.idProducto and idPedido=old.idPedido);
update Producto set Stock=Stock-cantProducto where idProducto=old.idProducto;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(5) NOT NULL,
  `Nombre` varchar(35) NOT NULL,
  `AP` varchar(35) NOT NULL,
  `AM` varchar(35) NOT NULL,
  `NombreComercial` varchar(35) NOT NULL,
  `Calle` varchar(35) NOT NULL,
  `Numero` varchar(35) NOT NULL,
  `Colonia` varchar(35) NOT NULL,
  `Municipio` varchar(35) NOT NULL,
  `Estado` varchar(35) NOT NULL,
  `CP` int(5) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Telefono` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `Nombre`, `AP`, `AM`, `NombreComercial`, `Calle`, `Numero`, `Colonia`, `Municipio`, `Estado`, `CP`, `Correo`, `Telefono`) VALUES
(1, 'Luis', 'Lopez', 'Jiménez', 'La Estrella', '2 de Abril', '2', 'Centro', 'Apizaco', 'Tlaxcala', 90491, 'laestrella@hotmail.com', 2411082351),
(3, 'kjhgfd', 'jhgfdx', 'hgfdxzs', 'ghfxd', 'hgfxdzs', '1', 'njbhvgcvf', 'bhvgcfxdz', 'Tlaxcala', 74123, 'jghvbcf', 741085296374);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `IdRol` int(5) NOT NULL,
  `Usuario` varchar(25) NOT NULL,
  `Contrasenia` varchar(25) NOT NULL,
  `Rol` enum('Dueño','Empleado') NOT NULL,
  `IdDuenio` int(5) DEFAULT NULL,
  `IdEmpleado` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`IdRol`, `Usuario`, `Contrasenia`, `Rol`, `IdDuenio`, `IdEmpleado`) VALUES
(1, 'FrankJV', '123456789', 'Empleado', NULL, 1),
(2, 'Fati123', '987654321', 'Dueño', 1, NULL),
(5, 'Frank1', '12345', 'Dueño', 2, NULL),
(7, 'MiriA', '1234', 'Dueño', 1, NULL),
(8, 'BrandonPB', '123456789', 'Empleado', NULL, 3),
(10, 'QuecholJE', '987654321', 'Dueño', 4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venpro`
--

CREATE TABLE `venpro` (
  `idProducto` int(5) NOT NULL,
  `idVenta` int(5) NOT NULL,
  `Cantidad` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venpro`
--

INSERT INTO `venpro` (`idProducto`, `idVenta`, `Cantidad`) VALUES
(1, 1, 3),
(1, 6, 1),
(3, 6, 2),
(1, 10, 1),
(1, 11, 2),
(3, 12, 1),
(3, 13, 1),
(1, 14, 5),
(3, 14, 2),
(1, 15, 1),
(3, 16, 1),
(1, 17, 1),
(1, 18, 1),
(1, 19, 1),
(1, 20, 1),
(1, 23, 1),
(7, 23, 1),
(1, 24, 1),
(3, 24, 1),
(1, 25, 1),
(3, 26, 1),
(1, 27, 1),
(1, 29, 2),
(7, 29, 2),
(1, 31, 1);

--
-- Disparadores `venpro`
--
DELIMITER $$
CREATE TRIGGER `aumentaProducto2` BEFORE DELETE ON `venpro` FOR EACH ROW begin
declare cantProducto int (5);
set cantProducto = (select Cantidad from VenPro where idProducto=old.idProducto and idVenta=old.idVenta);
update Producto set Stock=Stock+cantProducto where idProducto=old.idProducto;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `descProducto` AFTER INSERT ON `venpro` FOR EACH ROW begin
declare cantProducto int (5);
set cantProducto = (select Cantidad from VenPro where idProducto=new.idProducto and idVenta=new.idVenta);
update Producto set Stock=Stock-cantProducto where idProducto=new.idProducto;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idVenta` int(5) NOT NULL,
  `Fecha` date NOT NULL,
  `idEmpleado` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idVenta`, `Fecha`, `idEmpleado`) VALUES
(1, '2020-11-22', 1),
(2, '2020-11-22', 1),
(3, '2020-11-22', 1),
(4, '2020-11-15', 1),
(5, '2020-11-15', 1),
(6, '2020-11-22', 1),
(7, '2020-11-15', 1),
(8, '2020-11-15', 1),
(9, '2020-11-15', 1),
(10, '2020-11-22', 1),
(11, '2020-11-22', 1),
(12, '2020-11-23', 1),
(13, '2020-11-23', 1),
(14, '2020-11-23', 1),
(15, '2020-11-24', 1),
(16, '2020-11-24', 1),
(17, '2020-11-24', 1),
(18, '2020-11-24', 1),
(19, '2020-11-24', 1),
(20, '2020-11-24', 2),
(21, '2020-11-27', 2),
(22, '2020-11-27', 2),
(23, '2020-11-27', 2),
(24, '2020-11-27', 2),
(25, '2020-11-27', 2),
(26, '2020-11-27', 2),
(27, '2020-11-27', 2),
(28, '2020-11-29', 1),
(29, '2020-11-29', 2),
(30, '2020-11-30', 1),
(31, '2020-11-30', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `cortes`
--
ALTER TABLE `cortes`
  ADD PRIMARY KEY (`IdCorte`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indices de la tabla `duenio`
--
ALTER TABLE `duenio`
  ADD PRIMARY KEY (`IdDuenio`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `idVenta` (`idVenta`),
  ADD KEY `idCliente` (`idCliente`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`IdInventario`),
  ADD KEY `IdDuenio` (`IdDuenio`);

--
-- Indices de la tabla `invpro`
--
ALTER TABLE `invpro`
  ADD PRIMARY KEY (`IdProducto`,`IdInventario`),
  ADD KEY `IdInventario` (`IdInventario`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`idPedido`),
  ADD KEY `idProveedor` (`idProveedor`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `proped`
--
ALTER TABLE `proped`
  ADD PRIMARY KEY (`idPedido`,`idProducto`),
  ADD KEY `idProducto` (`idProducto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`IdRol`),
  ADD KEY `IdDuenio` (`IdDuenio`),
  ADD KEY `IdEmpleado` (`IdEmpleado`);

--
-- Indices de la tabla `venpro`
--
ALTER TABLE `venpro`
  ADD PRIMARY KEY (`idVenta`,`idProducto`),
  ADD KEY `idProducto` (`idProducto`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idVenta`),
  ADD KEY `idEmpleado` (`idEmpleado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cortes`
--
ALTER TABLE `cortes`
  MODIFY `IdCorte` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `duenio`
--
ALTER TABLE `duenio`
  MODIFY `IdDuenio` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idEmpleado` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `idFactura` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `IdInventario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `idPedido` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `IdRol` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idVenta` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cortes`
--
ALTER TABLE `cortes`
  ADD CONSTRAINT `cortes_ibfk_1` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`idVenta`) REFERENCES `venta` (`idVenta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_ibfk_3` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`IdDuenio`) REFERENCES `duenio` (`IdDuenio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `invpro`
--
ALTER TABLE `invpro`
  ADD CONSTRAINT `invpro_ibfk_1` FOREIGN KEY (`IdProducto`) REFERENCES `producto` (`idProducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invpro_ibfk_2` FOREIGN KEY (`IdInventario`) REFERENCES `inventario` (`IdInventario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proped`
--
ALTER TABLE `proped`
  ADD CONSTRAINT `proped_ibfk_1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `proped_ibfk_2` FOREIGN KEY (`idPedido`) REFERENCES `pedido` (`idPedido`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`IdDuenio`) REFERENCES `duenio` (`IdDuenio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venpro`
--
ALTER TABLE `venpro`
  ADD CONSTRAINT `venpro_ibfk_1` FOREIGN KEY (`idVenta`) REFERENCES `venta` (`idVenta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venpro_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
