<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class AMEG extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct(){
	parent::__construct();
	$this->load->model( 'Mpaciente');
	}
	
	 public function index()
	{

		$data['personas']=$this->Mpaciente->listar_doctores();
	
		$this->load->view('alumno/listar_alumno',$data);
		
	}
	public function logueadopa()
	{
		$data['personas1']=$this->Mpaciente->listar_alumnos();
		
		
		$this->load->view('alumno/listar_alumno2',$data);
	}

	public function logueadodr()
	{

		$data['personaDoctor']=$this->Mpaciente->obtener_datodr($this->session->idDoctor);


	
		$data['personas1']=$this->Mpaciente->listar_alumnos();
		$this->load->view('alumno/logueo_doctor',$data);
	}

	public function logueadoadm()
	{
		$data['personas1']=$this->Mpaciente->listar_alumnos();
		$this->load->view('alumno/iniciar_sesionAdm',$data);
	}

	public function agregar()
	{
		$variables=['mensaje'=>''];
		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
	
			
	//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
				
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
				
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
		$this->Mpaciente->insertar_alumno($data);
		$variables['mensaje']="ok";
		
		
		}
		$this->load->view('alumno/agregar_alumno',$variables);
	
	}

	public function agregardr()
	{
		$variables=['mensaje'=>''];
		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
			
	//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
				
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'Cedula'=> $this->input->post('Cedula',TRUE),
					'Especialidad'=> $this->input->post('Especialidad',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Calle'=> $this->input->post('calle',TRUE),
					'Municipio'=> $this->input->post('municipio',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
			$this->Mpaciente->insertar_doctor($data);
			$variables['mensaje']="ok";
		//	}
		//	else{

		//}
		
		} 
		$this->load->view('alumno/agregar_doctor',$variables);
	
	}

	public function iniciarse()
	{
		$variables=['mensaje'=>''];
					
		if(isset($_POST['Password'])){
			$this->load->model('Mpaciente');
			if($this->Mpaciente->login($_POST['Correo'],$_POST['Password'])){
				
			//	$this->session
			redirect('AMEG/logueadopa');
			}
			$variables['mensaje']="no";
		
		}

	
		$this->load->view('alumno/iniciar_sesion',$variables);
	}

	public function iniciarsedr()
	{
		$variables=['mensaje'=>''];
		if(isset($_POST['Password'])){
			$this->load->model('Mpaciente');
			if($this->Mpaciente->logindr($_POST['Correo'],$_POST['Password'])){
			redirect('AMEG/logueadoDr');
			}
			$variables['mensaje']="nodr";

		}

		$this->load->view('alumno/iniciar_sesiondr',$variables);
	}
	/*
	*funcion para editar a la persona
	*@param id
	return void
	*/

	public function solicitarCita(){
		$variables=['mensaje'=>''];

		$data['persona']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		$variables['personaPaciente']=$this->Mpaciente->obtener_dato($this->session->idPaciente);
		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Fecha',
					'label' => 'Fecha',
					'rules' => 'required'
				),
				array(
					'field' => 'Hora',
					'label' => 'Hora',
					'rules' => 'required'
				),

				array(
					'field' => 'Sintomas',
					'label' => 'Sintomas',
					'rules' => 'required'
				),
		
			)
			);

				if($this->form_validation->run() && $this->input->post()){
			
			//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
				//	{
						
						$data=array(
							
							'Fecha'=>$this->input->post('Fecha',TRUE),
							'Hora'=>$this->input->post('Hora',TRUE),
							'Sintomas'=>$this->input->post('Sintomas',TRUE),
							'Nombre'=>$this->input->post('Nombre',TRUE),
							'idPaciente' => $this->session->idPaciente,
							'idDoctor' => $this->input->post('idDoctor',TRUE)
							
						);
					
					//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
					$this->Mpaciente->insertar_cita($data);
					
					$variables['mensaje']="cita";
				//	}
				//	else{
		
				//}
				
				} 


				
				$variables ['personas']=$this->Mpaciente->mostrarr();

		

			$this->load->view('alumno/solicitar_cita',$variables);

	}

	public function deleteProfile(){
		$variables=['mensaje'=>''];

		//$data['persona']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		$this->Mpaciente->deleteProfile();

		$variables['mensaje']="cita";


	}

	



	public function editarpa($id = null)
	{
	

		$data['persona']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		
		if($this->form_validation->run() && $this->input->post()){
			
		//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
			
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
				
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
		//	$variables['mensaje']="seguropa";
		$this->alertas->db(	$this->Mpaciente->actualizar_alumno($data,$this->session->idPaciente),'AMEG/logueadopa');  

	//	}
	//	else{

	//	}
		
		} 

		$this->load->view('alumno/editar_alumno',$data);
		
	

	}

	public function editardr($id = null)
	{

		$data['persona']=$this->Mpaciente->obtener_datodr($this->session->idDoctor);


		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
			
		//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
			
				$data=array(
					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'CostoConsulta'=> $this->input->post('CostoConsulta',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Experiencia'=> $this->input->post('Experiencia',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
	
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
			$this->alertas->db($this->Mpaciente->actualizar_doctor($data,$this->session->idDoctor),'AMEG/logueadodr');  
	//	}
	//	else{

	//	}
		
		} 
		$this->load->view('alumno/editar_doctor',$data);
	

	}

	public function mostrarId($id = null)
	{

		$data['persona']=$this->Mpaciente->obtener_datodr($this->session->idDoctor);

		$this->load->view('alumno/logueo_doctor',$data);
	

	}





	public function eliminarPaciente (){
		

		$this->Mpaciente->eliminar_paciente($this->session->idPaciente);  
	}

	public function eliminarDr (){
		

		$this->Mpaciente->eliminar_doctor($this->session->idDoctor);  
	}

	function mostrar(){
		if($this->input->is_ajax_request()){
			
			$buscar=$this->input->post("buscar");
			
		
			$datos=	$this->Mpaciente->mostrar($buscar);

			
		 
	
			echo json_encode($datos);

	
		
			
		} else {

			show_404();
		}

	}
	
	function retornarDireccion(){
		if($this->input->is_ajax_request()){
			
			$buscar=$this->input->post("idDoctor");
			
	
			$datos=	$this->Mpaciente->direccion($buscar);

			
		 
	
			echo json_encode($datos);

	
		
			
		} else {

			show_404();
		}

	}


	
	function mostrarConsulta(){
		if($this->input->is_ajax_request()){
	
			
			$buscar=$this->input->post("buscar");
			
		
			$datos=	$this->Mpaciente->mostrarConsulta($buscar);
	
			echo json_encode($datos);
		
			
		} else {

			show_404();
		}

	}

	function mostrarConsultabyHora(){
		if($this->input->is_ajax_request()){
	
			
			$buscar=$this->input->post("buscar");
			
		
			$datos=	$this->Mpaciente->mostrarConsultabyHora($buscar);
	
			echo json_encode($datos);
		
			
		} else {

			show_404();
		}

	}
	function mostrarConsultabyNombre(){
		if($this->input->is_ajax_request()){
	
			
			$buscar=$this->input->post("buscar");
			
		
			$datos=	$this->Mpaciente->mostrarConsultabyNombre($buscar);
	
			echo json_encode($datos);
		
			
		} else {

			show_404();
		}

	}


	


	function enviarMensaje(){
		

		if($this->input->is_ajax_request()){

		$this->load->view('vendor2/autoload.php');
		require_once("assets/vendor/Pusher.php");
		
		//Desde la v2.2.0
		$options = array(
			'cluster' => 'mt1',
			'encrypt' => false
		  );
		  $pusher = new Pusher\Pusher(
			'766a096d82d621587061',
			'15c3c77bd3f943456eed',
			'1182731',
			$options
		  );

	//	  $elNombre=$this->input->post("notificacion");


		  $data['idDoctor']= $_POST["idSeleccionado"];
		  $data['message'] = $_POST["notificacion"];
		  $pusher->trigger('misNotificaciones', 'recibirNotificacion', $data);
		} else {
			show_404();
		}
	}


	public function mandarSMSPaciente (){
		// Copyright (c) 2020, Altiria TIC SL
		// All rights reserved.
		// El uso de este c�digo de ejemplo es solamente para mostrar el uso de la pasarela de env�o de SMS de Altiria
		// Para un uso personalizado del c�digo, es necesario consultar la API de especificaciones t�cnicas, donde tambi�n podr�s encontrar
		// m�s ejemplos de programaci�n en otros lenguajes y otros protocolos (http, REST, web services)
		// https://www.altiria.com/api-envio-sms/

		// XX, YY y ZZ se corresponden con los valores de identificacion del
		// usuario en el sistema.
		/*
		include('assets/vendor/httpPHPAltiria.php');

		$altiriaSMS = new AltiriaSMS();

		$altiriaSMS->setLogin('eduquezam1@gmail.com');
		$altiriaSMS->setPassword('ed21zAm94mR');


		$altiriaSMS->setDebug(true);

		$numeroConcatenado = "521".$POST['Numero'];
		echo $numeroConcatenado;
		

		//$sDestination = '346xxxxxxxx';
		$sDestination = '5212462550753';
		//$sDestination = array('346xxxxxxxx','346yyyyyyyy');
		$Concatenado = "¡Hola ".$_POST["Nombre"]."! Gracias por usar MediPro WEB";

		$response = $altiriaSMS->sendSMS($sDestination,'Hola' );
	*/

		include('assets/vendor/httpPHPAltiria.php');

		$altiriaSMS = new AltiriaSMS();

		$altiriaSMS->setLogin('eduquezam1@gmail.com');
		$altiriaSMS->setPassword('ed21zAm94mR');

		$altiriaSMS->setDebug(true);

		//Use this ONLY with Sender allowed by altiria sales team
		//$altiriaSMS->setSenderId('TestAltiria');
		//Concatenate messages. If message length is more than 160 characters. It will consume as many credits as the number of messages needed
		//$altiriaSMS->setConcat(true);
		//Use unicode encoding (only value allowed). Can send áéíóú but message length reduced to 70 characters
		//$altiriaSMS->setEncoding('unicode');

		//$sDestination = '346xxxxxxxx';
		$sDestination = '5212462550753';
		//$sDestination = array('346xxxxxxxx','346yyyyyyyy');

		$response = $altiriaSMS->sendSMS($sDestination, "Mensaje de prueba");

		if (!$response)
		echo "El envío ha terminado en error";
		else
		echo $response;

	/*	if (!$response)
		echo "El env�o ha terminado en error";
		else
		echo $response;*/
	}

	public function mandarSMS2(){

		$Concatenado = "¡Recordatorio!, Hola ".$_POST["Nombre"]." usted tiene una cita con el Dr. " . $_POST["elDr"]. " a las ".$_POST["HoraReal"] ;
		$ConcatenadoFecha = $_POST["Fecha"]." ".$_POST["Hora"].":00";
		$ConcatenadoNumero =  "52".$_POST["Numero"];
		echo $ConcatenadoFecha;

		echo $ConcatenadoFecha;
		//76b7e1c6796e44c28c001cf112ee9d1c Marco
		
		
		$request = '{
			"api_key":"cc1cf6ee48b94a598f2c648132eae820",
		
			"concat":1,
			"messages":[
				{
					"from":"MediPro Web",
					"to":"'.$ConcatenadoNumero.'",
					"text": "'.$Concatenado.'",
					"custom":"MyMsgID-12345",
					"send_at":"'.$ConcatenadoFecha.'"
				}
			]
		}';
		echo $request;
					
		$headers = array('Content-Type: application/json');        	
		
		$ch = curl_init('https://api.gateway360.com/api/3.0/sms/send');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		$result = curl_exec($ch);
		 
		if (curl_errno($ch) != 0 ){
			die("curl error: ".curl_errno($ch));
		}     
	}

	



}
 