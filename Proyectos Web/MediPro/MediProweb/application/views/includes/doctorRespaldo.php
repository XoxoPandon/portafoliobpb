
				<div class="owl-carousel owl-theme" id="testemonial-carousel">

<div class="home1-testm item">
	<div class="home1-testm-single text-center">
		<div class="home1-testm-img">
			<img src= "<?=base_url('assets/images/client/DS.png')?>" alt="img"/>
	  
		</div><!--/.home1-testm-img-->
		<div class="home1-testm-txt">
			<span class="icon section-icon">
				<i class="fa fa-quote-left" aria-hidden="true"></i>
			</span>
			<p>
			  DESCRIPCION D1		
		</p>
			<h3>
				<a href="#">
					DOCTOR 1
				</a>
			</h3>
			<h4>TLAXCALA</h4>
		</div><!--/.home1-testm-txt-->	
	</div><!--/.home1-testm-single-->

</div><!--/.item-->

<div class="home1-testm item">
	<div class="home1-testm-single text-center">
		<div class="home1-testm-img">
			<img src=      "<?=base_url('assets/images/client/DS.png')?>" alt="img"/>
		</div><!--/.home1-testm-img-->
		<div class="home1-testm-txt">
			<span class="icon section-icon">
				<i class="fa fa-quote-left" aria-hidden="true"></i>
			</span>
			<p>
		  DESCRIPCION D2	
		</p>
			<h3>
				<a href="#">
				   DOCTOR 2
				</a>
			</h3>
			<h4>TLAXCALA</h4>
		</div><!--/.home1-testm-txt-->	
	</div><!--/.home1-testm-single-->

</div><!--/.item-->

<div class="home1-testm item">
	<div class="home1-testm-single text-center">
		<div class="home1-testm-img">
			<img src=      "<?=base_url('assets/images/client/DS.png')?>" alt="img"/>
		</div><!--/.home1-testm-img-->
		<div class="home1-testm-txt">
			<span class="icon section-icon">
				<i class="fa fa-quote-left" aria-hidden="true"></i>
			</span>
			<p>
			 DESCRIPCION D3		
		</p>
			<h3>
				<a href="#">
					DOCTOR 3
				</a>
			</h3>
			<h4>TLAXCALA</h4>
		</div><!--/.home1-testm-txt-->	
	</div><!--/.home1-testm-single-->

</div><!--/.item-->

<div class="home1-testm item">
	<div class="home1-testm-single text-center">
		<div class="home1-testm-img">
			<img src=      "<?=base_url('assets/images/client/DS.png')?>" alt="img"/>
		</div><!--/.home1-testm-img-->
		<div class="home1-testm-txt">
			<span class="icon section-icon">
				<i class="fa fa-quote-left" aria-hidden="true"></i>
			</span>
			<p>
				 DESCRIPCION D4	
		   </p>
			<h3>
				<a href="#">
					DOCTOR SIMI
				</a>
			</h3>
			<h4>PUEBLA</h4>
		</div><!--/.home1-testm-txt-->	
	</div><!--/.home1-testm-single-->

</div><!--/.item-->

<div class="home1-testm item">
	<div class="home1-testm-single text-center">
		<div class="home1-testm-img">
			<img src=      "<?=base_url('assets/images/client/DS.png')?>" alt="img"/>
		</div><!--/.home1-testm-img-->
		<div class="home1-testm-txt">
			<span class="icon section-icon">
				<i class="fa fa-quote-left" aria-hidden="true"></i>
			</span>
			<p>
			  DESCRIPCION D5		
			 </p>
			<h3>
				<a href="#">
					 DOCTOR 5
				</a>
			</h3>
			<h4>TLAXCALA</h4>
		</div><!--/.home1-testm-txt-->	
	</div><!--/.home1-testm-single-->

</div><!--/.item-->



<div class="home1-testm item">
	<div class="home1-testm-single text-center">
		<div class="home1-testm-img">
			<img src="<?=base_url('assets/images/client/DS.png')?>" alt="img"/>
		</div><!--/.home1-testm-img-->
		<div class="home1-testm-txt">
			<span class="icon section-icon">
				<i class="fa fa-quote-left" aria-hidden="true"></i>
			</span>
			<p>
		   DESCRIPCION D6		
		</p>
			<h3>
				<a href="#">
					DOCTOR 6
				</a>
			</h3>
			<h4>TLAXCALA</h4>
		</div><!--/.home1-testm-txt-->	

	</div><!--/.home1-testm-single-->

</div><!--/.item-->

</div><!--/.testemonial-carousel-->
</div><!--/.container-->

</section><!--/.testimonial-->	
<!-- testemonial End -->
