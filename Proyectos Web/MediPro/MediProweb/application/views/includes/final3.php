	<!-- footer-copyright start -->
   

		

    <footer  class="footer-copyright">
			<div class="container">
				<div class="footer-content">
					<div class="row">

						<div class="col-sm-3">
							<div class="single-footer-item">
								<div class="footer-logo">
									<a href="index.html">
										Medi<span>Pro</span>
									</a>
									<p>
								Mejorando tu salud.
									</p>
								</div>
							</div><!--/.single-footer-item-->
						</div><!--/.col-->

						<div class="col-sm-3">
							<div class="single-footer-item">
								<h2>link</h2>
								<div class="single-footer-txt">
									<p><a href="#">Inicio</a></p>
									<p><a href="#res">Mis consultas</a></p>
								
								</div><!--/.single-footer-txt-->
							</div><!--/.single-footer-item-->

						</div><!--/.col-->

						<div class="col-sm-3">
							<div class="single-footer-item">
								<h2>Doctores con mejor reputación</h2>
								<div class="single-footer-txt">
									<p><a href="#">Doctor 1 </a></p>
									<p><a href="#">Doctor 2 </a></p>
									<p><a href="#">Doctor 3</a></p>
									<p><a href="#">Doctor 4</a></p>
									<p><a href="#">Doctor 5</a></p>
								</div><!--/.single-footer-txt-->
							</div><!--/.single-footer-item-->
						</div><!--/.col-->

						<div class="col-sm-3">
							<div class="single-footer-item text-center">
								<h2 class="text-left">contacts</h2>
								<div class="single-footer-txt text-left">
									<p>Calle Vicente Guerrero #1</p>
									<p class="foot-email"><a href="#">info@medipro.com</a></p>
									<p>Barrio Tepetlapa</p>
									<p>San Juan Totolac</p>
									<p>Tlaxcala. CP 90160</p>
								</div><!--/.single-footer-txt-->
							</div><!--/.single-footer-item-->
						</div><!--/.col-->

					</div><!--/.row-->

				</div><!--/.footer-content-->
				<hr>
				<div class="foot-icons ">
					<ul class="footer-social-links list-inline list-unstyled">
		                <li><a href="#" target="_blank" class="foot-icon-bg-1"><i class="fa fa-facebook"></i></a></li>
		                <li><a href="#" target="_blank" class="foot-icon-bg-2"><i class="fa fa-twitter"></i></a></li>
		                <li><a href="#" target="_blank" class="foot-icon-bg-3"><i class="fa fa-instagram"></i></a></li>
		        	</ul>
		        	<p>&copy; 2021 <a href="https://www.themesine.com"></a>. All Right Reserved</p>
					<p>&copy; Elaborado por:  <a href="https://www.themesine.com"></a> <br>Luis Francisco Jimenes Vigueras <br> Brandon Pilotzi Bautista
						<br>José Eduardo Zamora Quechol
					</p>

		        </div><!--/.foot-icons-->
				<div id="scroll-Top">
					<i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
				</div><!--/.scroll-Top-->
			</div><!-- /.container-->

		</footer><!-- /.footer-copyright-->
		<!-- footer-copyright end -->



	
        
		<!-- Include all compiled plugins (below), or include individual files as needed -->

		<!--modernizr.min.js-->
	
	
		
	
		<!--bootstrap.min.js -->
		<script  src="<?=base_url('assets/js/bootstrap.min.js')?>"></script> 
        
		<!-- bootsnav js -->
		<script src="<?=base_url('assets/js/bootsnav1.js')?>"></script>
    
		<!-- jquery.filterizr.min.js -->
		<script src= "<?=base_url('assets/js/jquery.filterizr.min1.js')?>"></script>
       
	

		<!--jquery-ui.min.js-->
        <script src=    "<?=base_url('assets/js/jquery-ui.min1.js')?>"></script>
    
        <!-- counter js -->
		<script src="<?=base_url('assets/js/jquery.counterup.min1.js')?>"></script>

		<script src="<?=base_url('assets/js/waypoints.min1.js')?>"></script>
        


		<!--owl.carousel.js-->
        <script  src= "<?=base_url('assets/js/owl.carousel.min1.js')?>"></script>
       

        <!-- jquery.sticky.js -->
		<script src="<?=base_url('assets/js/jquery.sticky1.js')?>"></script>


        <!--datepicker.js-->
        <script  src="<?=base_url('assets/js/datepicker1.js')?>"></script>


		<!--Custom JS-->
		<script src="<?=base_url('assets/js/custom1.js')?>"></script>
		
