<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Agregar</h4>
                                <ol class="breadcrumb">
                                    <li><a href="#">Alumno</a></li>
                               
                                </ol>
                            </div>
                        </div>
                        
                        <div class="row">
							
							
							<div class="col-lg-15">
								<div class="card-box">
									<h4 class="m-t-0 header-title"><b>Inserte sus datos completos</b></h4>
									<p class="text-muted font-13 m-b-30">
	                                    Todos sus datos serán confidenciales
	                                </p>
	                                
									<form class="form-horizontal" action="" method="post" role="form" data-parsley-validate="" novalidate="">

										<div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Nombre</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Nombre" id="Nombre" placeholder="Brenda" data-parsley-id="17" value="<?=$persona->Nombre ?>">
												<?=form_error('Nombre')?>
											</div>
										</div>

                                        <div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Apellido Paterno</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Apaterno" id="Apaterno" placeholder="Ortega" data-parsley-id="17" value="<?=$persona->Apaterno?>">
												<?=form_error('Apaterno')?>
											</div>
										</div>
                                        
                                        <div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Apellido Materno</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Amaterno" id="Amaterno" placeholder="Mendez" data-parsley-id="17" value="<?=$persona->Amaterno?>">
												<?=form_error('Amaterno')?>

											</div>
										</div>
                                       <br><br><br><br><br>     

                                        <div class="form-group3">
											<label for="inputEmail3" class="col-sm-4 control-label">Fecha de Nacimiento</label>
											<div class="col-sm-7">
												<input type="date" required="" parsley-type="date" class="form-control" name="Edad" id="Edad" data-parsley-id="17" value="2000-11-20">
												<?=form_error('Edad')?>
											</div>
										</div>
                                        <br><br><br>  

                                        <div class="form-group4">
											<label for="inputEmail3" class="col-sm-4 control-label">Usuario</label>
											<div class="col-sm-7">
												<input type="text" required="" parsley-type="text" class="form-control" name="Usuario" id="Usuario"   placeholder="Anonymous" data-parsley-id="17" value="<?=$persona->Usuario?>">
												<?=form_error('Usuario')?>
											</div>
										</div>
                                        <br><br><br>  

										<div class="form-group5">
											<label for="hori-pass1" class="col-sm-3 control-label">Password*</label>
											<div class="col-sm-2">
												<input id="hori-pass1" type="password" placeholder="Password" required="" name="Password" class="form-control" id="password" data-parsley-id="19" value="<?=$persona->Password?>">
												<?=form_error('Password')?>
											</div>
										</div>


										<div class="form-group5">
											<label for="hori-pass2" class="col-sm-3 control-label">Confirm Password *</label>
											<div class="col-sm-2">
												<input data-parsley-equalto="#hori-pass1" type="password" required="" name="password1" placeholder="Password" class="form-control" id="password1" data-parsley-id="21" value="<?=$persona->Password?>">
											</div>
										</div>
										
                                        <br><br><br>  
										<div class="form-group6">
											<div class="col-sm-offset-4 col-sm-8">
                                                <div class="checkbox">
                                                    <input id="remember-2" type="checkbox" data-parsley-multiple="remember-2" data-parsley-id="26">
                                                    <label for="remember-2">Recuérdame</label>
                                                </div>
											</div>
										</div>
                                        <br><br><br>  
										<div class="form-group">
											<div class="col-sm-offset-5 col-sm-8">
												<button  type="submit" class="btn btn-primary waves-effect waves-light">
													Registrar
												</button>
											
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
                        
                        
                        

    
    
     

            		</div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    dos mil veinte © MediPro.
                </footer>

            </div>