El siguiente proyecto fue creado con el framework codeignater y el uso de templetes.

La ruta para acceder a la vista principal es: "localhost/mediproweb/index.php/ameg"
Para su correcto funcionamiento es necesario cargar la base de datos que se adjunta a 
la carpeta del proyecto.

Para las pruebas es necesario iniciar sesión por lo que a continuación se proporciona un usuario:

Paciente:
	Correo: mauris.elit.dictum@doloregestasrhoncus.com
	Contraseña:MPM56MXU8MT

Doctor:
	Correo:felis.adipiscing.fringilla@dictum.co.uk
	Contraseña:RQY11KJF2LL

Realizado por: José Eduardo Zamora Quechol Brandon Pilotzi Bautista y Luis Jiménez Vigueras