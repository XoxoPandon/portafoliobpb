const contenedor = document.getElementById("test");
const botonRes = document.getElementById("boton");
const resultadoTest = document.getElementById("resultado");

contVisual = 0 ;
contAuditivo = 0 ;
contKine = 0;

const preguntas = [
  {
    pregunta: "1. ¿Cuál de las siguientes actividades disfrutas más?",
    respuestas: {
      a: "Escuchar música",
      b: "Ver películas",
      c: "Bailar con buena música",
	  
	  
    },
    visual1: 'b',
	auditivo1: 'a',
	kine1:'c',

  },
  {
    pregunta: "2. ¿Qué programa de televisión prefieres?",
    respuestas: {
      a: "Reportajes de descubrimientos y lugares",
      b: "Cómico y de entretenimiento",
      c: "Noticias del mundo",
    },
	visual1: "a",
	auditivo1: 'c',
	kine1:'b',
  },
  {
    pregunta: "3. Cuando conversas con otra persona, tú:",
    respuestas: {
      a: "La escuchas atentamente",
      b: "La observas",
      c: "Tiendes a tocarla ",
    },
	visual1: 'b',
	auditivo1: 'a',
	kine1:'c',
  },
  {
    pregunta: "4. Si pudieras adquirir uno de los siguientes artículos, ¿cuál elegirías? ",
    respuestas: {
      a: "Un jacuzzi",
      b: "Un estéreo",
      c: "Un televisor",
    },
	visual1: 'c',
	auditivo1: 'b',
	kine1:'a',
  },
  {
    pregunta: "5. ¿Qué prefieres hacer un sábado por la tarde?",
    respuestas: {
      a: "Quedarte en casa ",
      b: "Ir a un concierto ",
      c: "Ir al cine",
    },
	visual1: 'c',
	auditivo1: 'b',
	kine1:'a',
  },
  {
    pregunta: "6. ¿Qué tipo de exámenes se te facilitan más?	",
    respuestas: {
      a: "Examen oral",
      b: "Examen escrito",
      c: "Examen de opción múltiple",
    },
	visual1: 'b',
	auditivo1: 'a',
	kine1:'c',
  },
  {
    pregunta: "7. ¿Cómo te orientas más fácilmente?",
    respuestas: {
      a: "Mediante el uso de un mapa",
      b: "Pidiendo indicaciones",
      c: "A través de la intuición",
    },
	visual1: 'a',
	auditivo1: 'b',
	kine1:'c',
  },
  {
    pregunta: "8. ¿En qué prefieres ocupar tu tiempo en un lugar de descanso?",
    respuestas: {
      a: "Pensar",
      b: "Caminar por los alrededores",
      c: "Descansar",
    },
	visual1: 'b',
	auditivo1:'a',
	kine1:'c',
  },
  {
    pregunta: "9. ¿Qué te halaga más?",
    respuestas: {
      a: "Que te digan que tienes buen aspecto ",
      b: "Que te digan que tienes un trato muy agradable",
      c: "Que te digan que tienes una conversación interesante",
    },
	visual1: 'a',
	auditivo1: 'c',
	kine1:'b',
  },
  {
    pregunta: "10. ¿Cuál de estos ambientes te atrae más?",
    respuestas: {
      a: "Uno en el que se sienta un clima agradable",
      b: "Uno en el que se escuchen las olas del mar",
      c: "Uno con una hermosa vista al océano",
    },
	visual1: 'c',
	auditivo1: 'b',
	kine1:'a',
  },
];

function mostrarTest() {
  const preguntasYrespuestas = [];

  preguntas.forEach((preguntaActual, numeroDePregunta) => {
    const respuestas = [];

    for (letraRespuesta in preguntaActual.respuestas) {
		
      respuestas.push(
       		 ` <br> <font size=5>  <label style="color:white"  class="pservices text-justify" > 
                  <input  type="radio" name="${numeroDePregunta}" value="${letraRespuesta}" />
                  ${letraRespuesta} : ${preguntaActual.respuestas[letraRespuesta]} &nbsp&nbsp&nbsp
              </label>`
      );
    }

    preguntasYrespuestas.push(
      `  <font size=5>  <div   class="pservices text-justify"
 	  class="cuestion"  style="color:white"> ${preguntaActual.pregunta}</div>
          <div class="respuestas"> ${respuestas.join("")} <br><br> </div>
          `
    );
  });

  contenedor.innerHTML = preguntasYrespuestas.join("");
}

mostrarTest() ;

	function mostrarResultado() {
	const respuestas = contenedor.querySelectorAll(".respuestas");
	let respuestasCorrectas = 0;
	preguntas.forEach((preguntaActual, numeroDePregunta) => {

		const todasLasRespuestas = respuestas[numeroDePregunta];
		const checkboxRespuestas = `input[name='${numeroDePregunta}']:checked`;

		const respuestaElegida = (
			todasLasRespuestas.querySelector(checkboxRespuestas) || {}
		  ).value;

		if (respuestaElegida==preguntaActual.visual1){
			contVisual++;
		} else if (respuestaElegida==preguntaActual.auditivo1){
			contAuditivo++;
		}else if (respuestaElegida==preguntaActual.kine1){
			contKine++;
		}

	});

	
	if(contVisual>contAuditivo && contVisual>contKine ){
		respuestasCorrectas = "Visual"
	} else if (contAuditivo>contVisual && contAuditivo>contKine ){
		respuestasCorrectas = "Auditivo"
	} else if (contKine>contVisual && contKine>contAuditivo ){
		respuestasCorrectas = "Kinestésico"
	} else if (contVisual==contAuditivo){
		respuestasCorrectas = "Auditivo"
	} else if (contVisual==contKine){
		respuestasCorrectas = "Visual"
	} else if (contKine==contAuditivo){
		respuestasCorrectas = "Kinestésico"
	}



	$.ajax({
		url: "actualizarAprendizaje",
		type: "POST",
		data: {aprendizaje : respuestasCorrectas }
	}).done(function(debug_data) {
	//	alert(Nombre);
		console.log(respuestasCorrectas)
	});
	
  
	
	//	resultadoTest.style.color = "white"

	/*	alert(resultadoTest.innerHTML =
			"Usted es: " +
			respuestasCorrectas)*/
		
			Swal.fire({
				title: resultadoTest.innerHTML =
				"Usted es: " +
				respuestasCorrectas,
				width: 600,
				padding: '3em',
				background: '#fff url(/images/trees.png)',
				backdrop: `
				  rgba(0,0,123,0.4)
				  url("https://d1nr5wevwcuzuv.cloudfront.net/users/avatars/23935107/mktpl_large/343.gif")
				  left top
				  no-repeat
				`
			  }).then((result) => {
				if (result.isConfirmed) {
					
				
				//	localStorage.setItem("TipoAprendizaje",respuestasCorrectas)
				
					window.location.href = "http://127.0.0.1/cq/index.php/AMEG/logueado"
					
					/*$.ajax({
						url: "http://127.0.0.1/cq/index.php/AMEG/guardar",
						type: "POST",
						data: { data: respuestasCorrectas },
						success: function () {
							alert("respuestasCorrectas")
							console.log("hola");
							
						}
		
					});*/
					
				}
			})




}



botonRes.addEventListener("click", mostrarResultado);
