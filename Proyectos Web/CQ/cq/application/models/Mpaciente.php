<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Modelo alumnos
 * Este el modelo para el controlador alumno
 * @author José Eduardo Zamora Quechol
 */

 class Mpaciente extends CI_Model{
/**
 * Listar todos los alumnos
 * @return array
 */



public function listar_alumnos(){
    //select *from persona
    return $this->db->get('paciente')->result();
}

/**
 * Listar todos los alumnos
 * @param array
 * return boolean
 */
public function insertar_alumno($data){

    return $this->db->insert('paciente',$data);
}

/**
 * Listar todos los alumnos
 * @param array
 * return boolean
 */
public function insertar_doctor($data){
  
    return $this->db->insert('doctor',$data);
    
}

public function insertar_cita($data){

    return $this->db->insert('cita',$data);
}

public function login ($Correo,$Password){



    $this->db->where('Correo',$Correo);
    $this->db->where('Password',$Password);
    $q= $this->db->get('paciente')->row();
  //  $w = $q::fetch_array();

    if($q!=null){
        $q = (array)$q;
        $this->session->set_userdata($q);
        return true;
    }else{
        return false;
    }

}


public function logindr ($Correo,$Password){
    $this->db->where('Correo',$Correo);
    $this->db->where('Password',$Password);
    $q= $this->db->get('doctor')->row();
    if($q!=null){
        $q = (array)$q;
        $this->session->set_userdata($q);
        return true;
    }else{
        return false;
    }

}
/**
 * editar alumnos
 * return object
 */
public function obtener_dato($id){
  //  echo $id;
    $this->db->where('idPaciente',$id);
    return $this->db->get('paciente')->row();
}

public function retornar_Aprendizaje($id){


	$query = 	'select tipoAprendizaje from paciente where '.$id.' = idPaciente';
	
    $resultados = $this->db->query($query);


//	$this->db->like("Nombre",$valor);
//	$consulta = $this->db->get("doctor")->where("Estado from paciente"=="Estado from doctor");


	//return $consulta->result();

	return $resultados->result();
}


public function obtener_datodr($id){
  //  echo $id;
    $this->db->where('idDoctor',$id);

    return $this->db->get('doctor')->row();
}


/**
 * Actualizar datos del alumno
 * @param array
 * @param int
 * return boolean
 */
public function actualizar_alumno($data,$id){

$this->db->where('idPaciente',$id);
    return $this->db->update('paciente',$data);
}


public function actualizar_aprendizaje($data,$id){

	$this->db->where('idPaciente',$id);
		return $this->db->update('paciente',$data);
	}

public function actualizar_doctor($data,$id){

    $this->db->where('idDoctor',$id);
        return $this->db->update('doctor',$data);
    }

/**
 * Eliminar dato alumno
 * @param array
 * @param int
 * return boolean
 */
public function eliminar_alumno($id){
    $this->db->where('idPaciente',$id);
        return $this->db->delete('paciente');
    }


public function aprendizaje($data,$id){

	$this->db->where('idPaciente',$id);
	return $this->db->update('paciente',$data);
	}

 }
