<head> 

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 

		<!-- META DATA -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Rufina:400,700" rel="stylesheet" />

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet" />

		<!-- TITLE OF SITE -->
		<title>TECNO CLASS</title>

		<!-- favicon img -->
		<link rel="shortcut icon" type="image/icon" href="<?=base_url('assets/logo/favicon.png')?>" />
		

		<!--font-awesome.min.css-->
		<link rel="stylesheet" href="<?=base_url('assets/css/font-awesome1.min.css')?>" />
		
		<!--animate.css-->
		<link rel="stylesheet" href= "<?=base_url('assets/css/animate1.css')?>" />
		
		<!--hover.css-->
		<link rel="stylesheet" href="<?=base_url('assets/css/hover-min1.css')?>" />
		
		<!--datepicker.css-->
		<link rel="stylesheet"  href="<?=base_url('assets/css/datepicker1.css')?>"  />
		
		<!--owl.carousel.css-->
        <link rel="stylesheet" href="<?=base_url('assets/css/owl.carousel1.min.css')?>" />
	

		<link rel="stylesheet" href="<?=base_url('assets/css/owl.theme1.default.min.css')?>" />
		

		<!-- range css-->
        <link rel="stylesheet" href="<?=base_url('assets/css/jquery-ui1.min.css')?>"  />
	

		<!--bootstrap.min.css-->
		<link rel="stylesheet" href= "<?=base_url('assets/css/bootstrap1.min.css')?>" />


		<!-- bootsnav -->
		<link rel="stylesheet" href="<?=base_url('assets/css/bootsnav1.css')?>" />
		
		<link rel="stylesheet" href="<?=base_url('assets/css/Kinestesico.css')?>"  />
		<!--style.css-->
		<link rel="stylesheet" href="<?=base_url('assets/css/style5.css')?>"  />
		<link rel="stylesheet" href="<?=base_url('assets/css/style2.css')?>"  />
		<!--responsive.css-->
		<link rel="stylesheet" href="<?=base_url('assets/css/responsive1.css')?>"/>
		<link rel="stylesheet" href="<?=base_url('assets/css/estilosPDF.css')?>"/>



		 

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
