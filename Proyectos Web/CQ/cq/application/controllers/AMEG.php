<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AMEG extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
	parent::__construct();
	$this->load->model( 'Mpaciente');
	}
	
	 public function index()
	{
		$data['personas1']=$this->Mpaciente->listar_alumnos();
		$this->load->view('alumno/listar_alumno',$data);
	}
	public function logueado()
	{
		$data=['mensaje'=>''];

		$data['datosAlumno']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		$data['personas1']=$this->Mpaciente->listar_alumnos();



		$this->load->view('alumno/listar_alumno2',$data);
		
		
	}

	public function logueadodr()
	{
		$data['personas1']=$this->Mpaciente->listar_alumnos();
		$this->load->view('alumno/logueo_doctor',$data);
	}


	public function Auditivo()
	{

		$this->load->view('alumno/vistaAuditivo');
	}

	
	public function Auditivo1()
	{
	
		$this->load->view('alumno/vistaAuditivo1');
	}
	public function Auditivo2()
	{
	
		$this->load->view('alumno/vistaAuditivo2');
	}
	public function Auditivo3()
	{
	
		$this->load->view('alumno/vistaAuditivo3');
	}
	public function Auditivo4()
	{
	
		$this->load->view('alumno/vistaAuditivo4');
	}
	public function Auditivo5()
	{
	
		$this->load->view('alumno/vistaAuditivo5');
	}
	public function Auditivo6()
	{
	
		$this->load->view('alumno/vistaAuditivo6');
	}


	public function kinestesico()
	{
	
		$this->load->view('alumno/vistaKinestesico');
	}
	public function kinestesico1()
	{
	
		$this->load->view('alumno/vistaKinestesico1');
	}
	public function kinestesico2()
	{
	
		$this->load->view('alumno/vistaKinestesico2');
	}
	public function kinestesico3()
	{
	
		$this->load->view('alumno/vistaKinestesico3');
	}
	public function kinestesico4()
	{
	
		$this->load->view('alumno/vistaKinestesico4');
		
	}
	public function kinestesico5()
	{
	
		$this->load->view('alumno/vistaKinestesico5');
	}
	public function kinestesico6()
	{
	
		$this->load->view('alumno/vistaKinestesico6');
	}

	public function visual()
	{
	
		$this->load->view('alumno/vistaVisual');
	}
	
	public function visual2()
	{
	
		$this->load->view('alumno/vistaVisual2');
	}
	
	public function visual3()
	{
	
		$this->load->view('alumno/vistaVisual3');
	}
	
	public function visual4()
	{
	
		$this->load->view('alumno/vistaVisual4');
	}

	
	public function visual5()
	{
	
		$this->load->view('alumno/vistaVisual5');
	}
	
	public function visual6()
	{
	
		$this->load->view('alumno/vistaVisual6');
	}


	public function agregar()
	{
		$variables=['mensaje'=>''];
		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
	
			
	//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
				
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
				
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
		$this->Mpaciente->insertar_alumno($data);
		$variables['mensaje']="ok";
		
		
		}
		$this->load->view('alumno/agregar_alumno',$variables);
	
	}

	public function agregardr()
	{
		$variables=['mensaje'=>''];
		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
			
	//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
				
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'Cedula'=> $this->input->post('Cedula',TRUE),
					'Especialidad'=> $this->input->post('Especialidad',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
			$this->Mpaciente->insertar_doctor($data);
			$variables['mensaje']="ok";
		//	}
		//	else{

		//}
		
		} 
		$this->load->view('alumno/agregar_doctor',$variables);
	
	}

	public function iniciarse()
	{
		$variables=['mensaje'=>''];
					
		if(isset($_POST['Password'])){
			$this->load->model('Mpaciente');
			if($this->Mpaciente->login($_POST['Correo'],$_POST['Password'])){
				
			//	$this->session
			redirect('AMEG/logueado');
			}
			$variables['mensaje']="no";
		
		}

	
		$this->load->view('alumno/iniciar_sesion',$variables);
	}

	public function iniciarsedr()
	{
		$variables=['mensaje'=>''];
		if(isset($_POST['Password'])){
			$this->load->model('Mpaciente');
			if($this->Mpaciente->logindr($_POST['Correo'],$_POST['Password'])){
			redirect('AMEG/logueadoDr');
			}
			$variables['mensaje']="nodr";

		}

		$this->load->view('alumno/iniciar_sesiondr',$variables);
	}
	/*
	*funcion para editar a la persona
	*@param id
	return void
	*/

	public function realizarTest(){
		$variables=['mensaje'=>''];

		$data['persona']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Fecha',
					'label' => 'Fecha',
					'rules' => 'required'
				),
				array(
					'field' => 'Hora',
					'label' => 'Hora',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Direccion',
					'label' => 'Direccion',
					'rules' => 'required'
				),
				array(
					'field' => 'Sintomas',
					'label' => 'Sintomas',
					'rules' => 'required'
				),
		
			)
			);

				if($this->form_validation->run() && $this->input->post()){
			
			//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
				//	{
						
						$data=array(
		
							'Fecha'=>$this->input->post('Fecha',TRUE),
							'Hora'=>$this->input->post('Hora',TRUE),
							'Direccion'=>$this->input->post('Direccion',TRUE),
							'Sintomas'=>$this->input->post('Sintomas',TRUE)
							
						);
					
					//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
					$this->Mpaciente->insertar_cita($data);
					$variables['mensaje']="cita";
				//	}
				//	else{
		
				//}
				
				} 
				$this->load->view('alumno/solicitar_cita',$variables);

	}

	



	public function editarpa($id = null)
	{
	

		$data['persona']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		
		if($this->form_validation->run() && $this->input->post()){
			
		//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
			
				$data=array(

					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE),
	
				
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
		//	$variables['mensaje']="seguropa";
		$this->alertas->db(	$this->Mpaciente->actualizar_alumno($data,$this->session->idPaciente),'AMEG/logueado');  

	//	}
	//	else{

	//	}
		
		} 
		
		$this->load->view('alumno/editar_alumno',$data);
	

	}

	public function actualizarAprendizaje($id = null)
	{
	//	$data['persona']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		if($this->input->is_ajax_request()){


			  $data['tipoAprendizaje']= $_POST["aprendizaje"];


			} else {
				show_404();
			}
	

	
		$this->alertas->db(	$this->Mpaciente->actualizar_aprendizaje($data,$this->session->idPaciente),'AMEG/logueado');  
		
	
		
	//	$this->load->view('alumno/editar_alumno',$data);
	

	}

	public function editardr($id = null)
	{

		$data['persona']=$this->Mpaciente->obtener_datodr($this->session->idDoctor);

		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'Nombre',
					'label' => 'Nombre',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required'
				),
				
				array(
					'field' => 'Amaterno',
					'label' => 'Apelllido materno',
					'rules' => 'required'
				),
				array(
					'field' => 'Edad',
					'label' => 'Edad',
					'rules' => 'required'
				),
				array(
					'field' => 'Correo',
					'label' => 'Usuario',
					'rules' => 'required'
				),
				array(
					'field' => 'Password',
					'label' => 'Contraseña',
					'rules' => 'required'
				),
			)
			);
		if($this->form_validation->run() && $this->input->post()){
			
		//	if($this->input->post('password',TRUE)==$this->input->post('password1',TRUE))
		//	{
			
				$data=array(
					'Nombre'=>$this->input->post('Nombre',TRUE),
					'Apaterno'=>$this->input->post('Apaterno',TRUE),
					'Amaterno'=>$this->input->post('Amaterno',TRUE),
					'FN'=> $this->input->post('Edad',TRUE),
					'CostoConsulta'=> $this->input->post('CostoConsulta',TRUE),
					'Telefono'=> $this->input->post('Telefono',TRUE),
					'Estado'=> $this->input->post('Estado',TRUE),
					'Sexo'=> $this->input->post('Sexo',TRUE),
					'Experiencia'=> $this->input->post('Experiencia',TRUE),
					'Correo'=> $this->input->post('Correo',TRUE),
					'Password'=>$this->input->post('Password',TRUE)
	
	
	
				);
	
			//	$this->alertas->db($this->Malumnos->insertar_alumno($data),'Eduardo/agregar');
			$this->alertas->db($this->Mpaciente->actualizar_doctor($data,$this->session->idDoctor),'AMEG/logueadodr');  
	//	}
	//	else{

	//	}
		
		} 
		$this->load->view('alumno/editar_doctor',$data);

	}


	public function eliminar ($id = null){
		$this->alertas->db($this->Mpaciente->eliminar_alumno($id),'Eduardo');  
	}

	function guardar(){
		$data['persona']=$this->Mpaciente->obtener_dato($this->session->idPaciente);

		$usuario = $_POST['respuestasCorrectas'];
		
		console.log($usuario);
	//	echo "tu usuario es: ".$usuario; 
	

	/*	if($this->input->is_ajax_request()){
			
			$data=$this->input->post("data");
			
		
			$datos=	$this->Mpaciente->aprendizaje($data,$this->session->idPaciente);
	
			echo json_encode($datos);
		
			
		} else {
		
			show_404();
		}
	*/
	}



}
 